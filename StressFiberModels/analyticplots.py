'''
Created on Feb 1, 2014

@author: fogelson
'''

import operator
import os.path
import numpy as np
import NeuroTools.parameters as par
import sys
import matplotlib.pyplot as plt

def v(x, alpha, beta, mtot):
    # Analytic solution to stress equation for v with myosin concentration
    # m(x) = mtot*delta(x)
    
    # Solve particular solution with a Fourier transform
    # Solve homogeneous easily
    
    c1 = mtot*np.exp(-np.sqrt(alpha))*(beta - np.sqrt(alpha))/(2*(beta*np.sinh(np.sqrt(alpha)) + np.sqrt(alpha)*np.cosh(np.sqrt(alpha))))

    vp = mtot*np.where(x >= 0, -0.5*np.exp(-np.sqrt(alpha)*x), 0.5*np.exp(np.sqrt(alpha)*x))
    vh = c1*np.sinh(np.sqrt(alpha)*x)
    
    return vp + vh

def main():
    options = sys.argv[1:]
    setup = par.ParameterSet(options[0])
    label = options[1]
    
    savefolder = os.path.join('./Data', label)

    # Total myosin amount attached to fiber
    mtot = setup.mtot
    
    # Lengths to calculate force over
    Lmin = setup.length.Lmin
    Lmax = setup.length.Lmax
    Lnum = setup.length.Lnum
    L = np.linspace(Lmin, Lmax, Lnum)
    
    # Scale for alpha = alphaCoeff * L**2
    alphaCoeff = setup.length.alphaCoeff
    
    # Scale for beta = betaCoeff * L
    betaCoeff = setup.length.betaCoeff
    
    if type(alphaCoeff) == float or type(alphaCoeff) == int:
        alphaCoeff = [alphaCoeff]
    if type(betaCoeff) == float or type(betaCoeff) == int:
        betaCoeff = [betaCoeff]
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    for ac in alphaCoeff:
        for bc in betaCoeff:
            alphas = ac*(L**2)
            betas = bc*L
            vl = map(lambda a, b: v(-1, a, b, mtot), alphas, betas)
            fl = betas*vl
            ax.plot(L, fl)
    ax.set_xlabel('Length')
    ax.set_ylabel('Force on substrate')
    
    figfilename = os.path.join(savefolder, 'analytic_force_vs_length.pdf')
    fig.savefig(figfilename)
    plt.close(fig)
    
    del alphas, betas
    
    Na = setup.parameterspace.Na
    alphaMin = setup.parameterspace.alphaMin
    alphaMax = setup.parameterspace.alphaMax
    
    Nb = setup.parameterspace.Nb
    betaMin = setup.parameterspace.betaMin
    betaMax = setup.parameterspace.betaMax
    
    alphas = np.linspace(alphaMin, alphaMax, Na)
    betas = np.linspace(betaMin, betaMax, Nb)
    
    A = np.zeros((Na, Nb))
    B = np.zeros((Na, Nb))
    F = np.zeros((Na, Nb))
    
    for ia in xrange(Na):
        for ib in xrange(Nb):
            alpha = alphas[ia]
            beta = betas[ib]
            A[ia, ib] = alpha
            B[ia, ib] = beta
            vl = v(-1, alpha, beta, mtot)
            F[ia, ib] = beta*vl
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    ax.pcolor(A, B, F)
    
    ax.set_xlabel('alpha')
    ax.set_ylabel('beta')
    
    ax.set_title('Force at fiber edge')
    
    figfilename = os.path.join(savefolder, 'analytic_force_vs_alpha_and_beta.pdf')
    fig.savefig(figfilename)
    plt.close(fig)
    
    
    

if __name__ == '__main__':
    main()