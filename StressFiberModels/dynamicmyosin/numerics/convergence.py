'''
Created on Feb 23, 2014

@author: fogelson
'''

import abc
from .util import *
from . import geometry
from scipy.interpolate import UnivariateSpline
import numpy.linalg as la
import multiprocessing as mp
import NeuroTools.parameters as par

def zipParams(setup):
    '''
    Looks at a parameter file, tries to find a convergence
    section. In that section, there should be a subsection
    for each parameter that is to be iterated over. In
    each of those subsections, there should be two entries:
    name, which is a string giving the full name of the
    parameter in this current file that should be modified,
    and vals, which should be a list of the values that
    the parameter should take in subsequent iterations of
    the convergence study.
    
    Returns a zipped list of tuples, each tuple's first
    entry is the full name of that param, and the second
    entry is the list vals.
    '''
    names = setup.convergence.vars.keys()
    fullNames = map(lambda n: setup.convergence.vars[n].name, names)
    ranges = map(lambda n: setup.convergence.vars[n].vals, names)
    return zip(fullNames, ranges)

class ConvergenceTester(object):
    '''
    Class for testing convergence of numerical methods.
    '''
    
    def __init__(self, solveMethod, setup):
        '''
        solveMethod should be a function of the form solveMethod(setup),
        which returns an instance of the Solution class.
        
        paramRange should be a dict whose keys are strings and whose
        values are lists.
        
        '''
        self.solve = solveMethod
        self.zippedParams = zipParams(setup)
        self.setup = setup

    def compute_all_solutions(self):
        '''
        Iterate over the range of parameters in self.paramRange,
        computing solutions, and storing the results in self.sols
        '''
        # Number of solutions we expect is given by the number
        # of values of the parameter first in the paramRange dict.
        # Note that there is no error checking for ensuring that
        # all parameter ranges in the dict have the same length.
        # But they should.
        
        sols = []
        zippedParams = self.zippedParams
        numSols = len(zippedParams[0][1])
        
        numProc = 1
        if self.setup.convergence.has_key('processes'):
            numProc = self.setup.convergence.processes
        
        setups = []
        for i in xrange(numSols):
            setup = par.ParameterSet(self.setup.pretty())
            for name, vals in zippedParams:
                val = vals[i]
                setup[name] = val
            setups.append(setup)

        if numProc == 1:
            for setup in setups:
                sols.append(self.solve(setup))
        else:
            pool = mp.Pool(numProc)
            # Assumes the more intensive simulations are at
            # the end of the list, so should be started first
            setups = setups.reverse()
            sols = pool.map(self.solve, setups)
            # Unreverses both setups (just in case of future changes
            # and sols
            setups = setups.reverse()
            sols = sols.reverse()
            
        self.sols = sols

class Solution(object):
    '''
    Simple structure for storing a particular simulation result.
    '''
    def __init__(self, x, y):
        self.x = x
        self.y = y

def interpolate(xOut, xIn, yIn):
    '''
    Use a cubic spline to interpolate from the points
    (xIn, yIn) to (xOut, yOut). Returns yOut.
    
    Typically will be used to map grid values on a finer
    grid to a coarser grid, or vice versa.
    '''
    spline = UnivariateSpline(xIn, yIn, s = 0)
    yOut = spline(xOut)
    return yOut

def compute_error(sol1, sol2, order=1):
    '''
    Computes the error in the given grid norm between two solutions.
    If those solutions are on different grids, uses
    the interpolate() function to restrict the solution on
    the finer grid to the coarser grid before comparison.
    '''
    N1 = len(sol1.x)
    N2 = len(sol2.x)
    if N1 <= N2:
        N = N1
        x = sol1.x
    else:
        N = N2
        x = sol2.x
    
    def restrictSol(sol):
        if len(sol.x) != N:
            y = interpolate(x, sol.x, sol.y)
            sol = Solution(x, y)
        return sol
    
    sols = map(restrictSol, (sol1, sol2))
    
    h = geometry.get_h(N)
    absdiff = abs(sols[0].y - sols[1].y)
    if order == np.inf:
        err = absdiff.max()
    else:
        err = (h*sum(absdiff**order))**(1.0/order)
    return err

def error_ratios(sols, order=1):
    '''
    Takes as input a list of solutions, sols, of length M >= 3.
    
    For each adjacent pair of solutions in sols, uses
    the compute_error() function to calculate the difference
    between those solutions.
    
    For each of adjacent pair of differences, computes the ratio
    between the two difference, returning a list of M - 2 ratios.
    '''
    M = len(sols)
    E = np.zeros((M - 1))
    for m in xrange(M - 1):
        sol1 = sols[m]
        sol2 = sols[m + 1]
        err = compute_error(sol1, sol2, order)
        E[m] = err
    R = E[0:-1]/E[1:]
    return R