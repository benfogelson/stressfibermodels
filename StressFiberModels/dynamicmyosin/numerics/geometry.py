'''
Created on Jan 27, 2014

@author: fogelson
'''

import numpy as np

# For now N is hard set, but one eventual goal is to have N
# as well as the nondimensional parameters in physicalsetup.py
# automatically read from a config file instead of being hard
# set in the module. Sounds like something future Ben can handle.
# N = 128
# deltaT = .1

def get_h(N):
    '''
    If the interval [-1/2,1/2] is divided into N equal-width subintervals,
    get_h(N) returns the width of each subinterval.
    '''
    return 1.0/N

def get_cell_centers(N):
    '''
    Splits the interval [-1/2,1/2] into N equal-width subintervals
    and returns an array giving the coordinate of the
    center of each cell.
    
    In documentation, these coordinates are referred to as
    x_0, x_1, x_2, ..., x_{N-1}.
    '''
    
    h = get_h(N)
    
    x = np.arange(N)*h - 0.5 + h/2
    
    return x
    
def get_cell_edges(N):
    '''
    Returns all N+1 cell edge coordinates for the N subintervals
    from [-1,1]. These are referred to by indices x_{-1/2}, x_{1/2},
    x_{3/2}, ..., x_{N-1/2}
    '''
    
    h = get_h(N)
    
    x = np.arange(N+1)*h - 0.5
    
    return x

def get_left_cell_edges(N):
    '''
    Returns the N left cell edges of the subintervals.
    '''
    x = get_cell_edges(N)
    return x[0:N]

def get_right_cell_edges(N):
    '''
    Returns the N right cell edges of the subintervals.
    '''
    x = get_cell_edges(N)
    return x[1:N+1]