'''
Created on Feb 6, 2014

@author: fogelson
'''

from dynamicmyosin.numerics.util import *
from dynamicmyosin.numerics import geometry


class Initializer(object):
    def __init__(self, setup):
        self.setup = setup
        self.x = geometry.get_cell_centers(setup.numerical.N)
    
    def get_v(self):
        return np.zeros(self.x.shape)

    def get_m(self):
        method = self.setup.initial.m
        if method == "rand":
            return np.random.rand(len(self.x))
        elif method == "ones":
            return np.ones(self.x.shape)
    
    def get_n(self):
        return np.zeros(self.x.shape)