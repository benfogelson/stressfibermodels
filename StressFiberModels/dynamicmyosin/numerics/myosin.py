'''
Created on Jan 27, 2014

@author: fogelson
'''

from dynamicmyosin.numerics.util import *
from dynamicmyosin.numerics import geometry
from dynamicmyosin.numerics.myosinadvection import cell_to_face, compute_flux
import logging

def make_linear_difference_operator(setup):
    """
    Linear finite difference operator for
    the reaction diffusion differential operator
    for both bound and free myosin
    """
    N = setup.numerical.N
    h = geometry.get_h(N)
    
    delta = setup.physical.delta
    delta_small = setup.physical.delta_small
    gamma1 = setup.physical.gamma1
    gamma2 = setup.physical.gamma2
    
    h2 = h**2
    
    lower = np.ones((N-1))/h2
    middle = -2*np.ones((N))/h2
    upper = np.ones((N-1))/h2
    L = sparse.diags([lower, middle, upper], [-1, 0, 1])
    
    L = L.tolil()
    
    L[0, :] = 0
    L[N-1, :] = 0

    L[0, 0] = -1/h2
    L[0, 1] = 1/h2

    L[N-1, N-2] = 1/h2
    L[N-1, N-1] = -1/h2
    
    I = sparse.eye(N, N)
    
    B = sparse.bmat([[delta_small*L - gamma1*I, gamma2*I]])
    
    F = sparse.bmat([[gamma1*I, delta*L - gamma2*I]])
    
    A = sparse.bmat([[B], [F]])
    
    return A


def iteratefull(m0, n0, v, setup, A=None, timestepfactor=1):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    deltaT = setup.numerical.deltaT * timestepfactor
    N = setup.numerical.N
    h = geometry.get_h(N)

    # Compute velocities at interior faces
    v_face = cell_to_face(v)

    # Check CFL condition, use a shorter timestep if necessary
    c_curr = np.max(abs(v_face) * deltaT / h)
    c_max = 0.9
    if c_curr > c_max:
        m = m0

        # New multiplicative factor for shorter timesteps
        short_time_factor = c_max / c_curr

        # Number of new timesteps at courant number c_max
        steps = int(np.floor(c_curr / c_max))
        for k in xrange(steps):
            m, n = iteratefull(m, n0, v, setup, timestepfactor=timestepfactor * short_time_factor)

        # One final timestep
        c_final = c_curr - steps * c_max
        final_time_factor = c_final / c_curr
        m, n = iteratefull(m, n0, v, setup, timestepfactor=timestepfactor * final_time_factor)
    else:
        F0 = compute_flux(m0, v, setup, timestepfactor)
        C0 = (F0[1:N+1] - F0[0:N])/h
        C0 = np.append(C0, 0*C0)

        if A is None:
            A = make_linear_difference_operator(setup)

        I = sparse.eye(2*N, 2*N)

        method = setup.numerical.solvers.myosin
        if method == 'CN' or method == 'BE':
            raise NotImplementedError('Not implemented for CN or BE')
            # if method == 'CN':
            #     LHS = I - (deltaT/2)*A
            #     RHS = I + (deltaT/2)*A
            #
            # elif method == 'BE':
            #     # Backward Euler
            #     LHS = I - deltaT*A
            #     RHS = I

            a0 = np.append(m0, n0)
            a = spla.spsolve(LHS, RHS*a0)
            m = a[0:N]
            n = a[N:]
        elif method == 'TR-BDF2':
            # TR-BDF2 (LeVeque's Finite Difference Methods, page 175)
            # Modified to treat the nonlinear flux term explicitly
            LHS = I - (deltaT/4)*A
            RHS = I + (deltaT/4)*A
            a0 = np.append(m0, n0)
            b0 = (RHS*a0) - (deltaT/2)*C0
            astar = spla.spsolve(LHS, b0)
            bstar = (4*astar - a0)/3 - (deltaT/3)*C0
            LHSstar = I - (deltaT/3)*A
            a1 = spla.spsolve(LHSstar, bstar)
            m = a1[0:N]
            n = a1[N:]

    return m, n


def iterate(m0, n0, setup, A=None, timestepfactor=1):
    """
    Solve the system. Uses the given coefficient matrix A
    for the finite difference representation of the PDE,
    and computes A if A is None
    
    By default, uses the timestep deltaT given in
    dynamicmyosin.setup, but this can be modified by
    the factor timestepfactor. So timestepfactor=0.5
    would cause the timestep to be 05*deltaT.
    """
    deltaT = setup.numerical.deltaT*timestepfactor
    N = setup.numerical.N

    if A is None:
        A = make_linear_difference_operator(setup)
    
    I = sparse.eye(2*N, 2*N)
    
    method = setup.numerical.solvers.myosin
    if method == 'CN' or method == 'BE':
        if method == 'CN':
            # Crank-Nicholson
            LHS = I - (deltaT/2)*A
            RHS = I + (deltaT/2)*A
        
        elif method == 'BE':
            # Backward Euler
            LHS = I - deltaT*A
            RHS = I
        
        a0 = np.append(m0, n0)
        a = spla.spsolve(LHS, RHS*a0)
        m = a[0:N]
        n = a[N:]
    elif method == 'TR-BDF2':
        # TR-BDF2 (LeVeque's Finite Difference Methods, page 175)
        LHS = I - (deltaT/4)*A
        RHS = I + (deltaT/4)*A
        a0 = np.append(m0, n0)
        astar = spla.spsolve(LHS, RHS*a0)
        b0 = (4*astar - a0)/3
        LHS2 = I - (deltaT/3)*A
        b = spla.spsolve(LHS2, b0)
        m = b[0:N]
        n = b[N:]
    
    return m, n
    