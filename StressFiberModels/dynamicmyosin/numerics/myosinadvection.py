'''
Created on Jan 31, 2014

@author: fogelson
'''

from .util import *
from . import geometry
import logging

def cell_to_face(v):
    """
    Interpolate the cell centered quantity v to
    the faces of each cell using linear interpolation
    of the two neighboring faces. This just means
    we take the average of the two neighboring faces.
    """
    N = len(v)
    v_face = (v[1:N] + v[0:N-1])/2
    return v_face

def iterate(m0, v, setup, timestepfactor=1):
    deltaT = setup.numerical.deltaT*timestepfactor
    N = setup.numerical.N
    h = geometry.get_h(N)
    
    # Compute velocities at interior faces
    v_face = cell_to_face(v)
    
    # Check CFL condition, use a shorter timestep if necessary
    c_curr = np.max(abs(v_face)*deltaT/h)
    c_max = 0.9
    if c_curr > c_max:
        m = m0
        
        # New multiplicative factor for shorter timesteps
        short_time_factor = c_max/c_curr
        
        # Number of new timesteps at courant number c_max
        steps = int(np.floor(c_curr/c_max))
        for k in xrange(steps):
            m = iterate(m, v, setup, timestepfactor=timestepfactor*short_time_factor)
        
        # One final timestep
        c_final = c_curr - steps*c_max
        final_time_factor = c_final/c_curr
        m = iterate(m, v, setup, timestepfactor=timestepfactor*final_time_factor)
    else:
        F = compute_flux(m0, v, setup, timestepfactor)
        
        # Advance using flux differencing
        m = m0 - (deltaT/h)*(F[1:N+1] - F[0:N])
    return m


def compute_flux(m0, v, setup, timestepfactor=1):
    '''
    Compute the flux differencing that results from using either
    Godunov's method or a high-resolution flux limiting method.
    This is essentially the spatial discretization of the PDE.
    '''
    method = setup.numerical.solvers.myosinadvection
    if method == 'High-res':
        func = compute_flux_highres
    elif method == 'Godunov':
        func = compute_flux_godunov
    return func(m0, v, setup, timestepfactor)


def compute_flux_highres(m0, v, setup, timestepfactor=1):
    deltaT = setup.numerical.deltaT*timestepfactor
    N = setup.numerical.N
    h = geometry.get_h(N)
    
    # Compute velocities at interior faces
    v_face = cell_to_face(v)
    
    # Number of ghost cells
    num_ghost = 2
    # Indices of interior cells of the ghost cell
    # padded array
    interior_cells = np.arange(num_ghost, N+num_ghost)
    
    # Extended m
    m = np.zeros(N+2*num_ghost)
    m[interior_cells] = m0
    
    # Set ghost cells
    
    # Odd extension
    # m[0:num_ghost] = -m[2*num_ghost-1:num_ghost-1:-1]
    # m[N+num_ghost:N+2*num_ghost] = -m[N+num_ghost-1:N-1:-1]

    # Constant extension
    m[0:num_ghost] = m[num_ghost]
    m[N+num_ghost:N+2*num_ghost] = m[N+num_ghost-1]

    vm = np.where(v_face < 0, v_face, 0)
    vp = np.where(v_face > 0, v_face, 0)
    
    # Compute jumps in m over interfaces, and
    # upwind jumps over interfaces
    delM = m[interior_cells[1:N]] - m[interior_cells[0:N-1]]
    delM_uw = np.where(v_face > 0, m[interior_cells[0:N-1]] - m[interior_cells[0:N-1]-1], m[interior_cells[1:N]+1] - m[interior_cells[1:N]])
    
    # Ratio of upwind jump to jump at interface
    theta = np.where(delM != 0, delM_uw/delM, 0.0)
    
    # Apply limiter to theta, currently minmod
    lim = lambda th: np.maximum(0, np.minimum(1, 2*th), np.minimum(2, th)) #Superbee
    #minmod = lambda a, b: np.where(a*b > 0, np.minimum(np.abs(a), np.abs(b)), 0)
    #lim = lambda th: minmod(1.0, th) #Minmod
    #lim = lambda th: 0.0*th
    #lim = lambda th: np.ones(np.shape(th)) # Lax-Wendroff
    delta = lim(theta)*delM
    
    # Compute flux with no flux boundary conditions and limited flux in interior
    F = np.zeros(N+2)
    F[1:N] = vm*m[interior_cells[1:N]] + vp*m[interior_cells[0:N-1]] + 0.5*np.abs(v_face)*(1 - np.abs(v_face*deltaT/h))*delta

    return F


def compute_flux_godunov(m0, v, setup, timestepfactor=1):
    """
    Computes one iteration of the
    advection equation dm/dt = -d(v*m)/dx.
    
    By default, uses the timestep deltaT given in
    dynamicmyosin.setup, but this can be modified by
    the factor timestepfactor. So timestepfactor=0.5
    would cause the timestep to be 05*deltaT.
    
    Currently uses upwinding, so this is only a first order
    method. Ways to improve: higher order method or and/or
    a higher order limiter.
    
    """
    
    N = setup.numerical.N
    
    # Compute flow velocity at cell faces
    v_face = cell_to_face(v)

    # Compute upwind direction
    sign_v = np.sign(v_face)
    m_uw = np.where(sign_v >=0, m0[0:N-1], m0[1:N])
    
    # Compute fluxes over all N+1 faces, using no flux
    # boundary conditions at edges
    F = np.zeros((N+1))
    F[1:N] = m_uw*v_face
    
    return F
    