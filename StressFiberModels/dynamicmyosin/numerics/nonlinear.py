'''
Created on Feb 24, 2014

@author: fogelson
'''

from .util import *
from . import myosin
from . import myosinadvection
from . import velocity
from . import geometry
from scipy import optimize, interpolate

def create_flux_function(setup, k=1):
    L = velocity.make_linear_difference_operator(setup)
    Lsolver = sparse.linalg.factorized(L)    
    
    N = setup.numerical.N
    x_cell = geometry.get_cell_centers(N)
    x_edge = geometry.get_cell_edges(N)
    
    def cell_to_face(y, k=k):
        spline = interpolate.UnivariateSpline(x_cell, y, k=k)
        return spline(x_edge)
    
    def F(m):
        rhs = velocity.make_rhs(m, setup)
        v_cell = Lsolver(rhs)
        v_edge = cell_to_face(v_cell)
        m_edge = cell_to_face(m)
        f = v_edge*m_edge
        f[0] = 0
        f[-1] = 0
        return f
    
    #def F(m):
    #    rhs = velocity.make_rhs(m, setup)
    #    v_cell = Lsolver(rhs)
    #    out = myosinadvection.compute_flux(m, v_cell, setup)
    #    return out
    
    return F



# def create_flux_function(setup):
#     '''
#     Create nonlinear flux function F(m) that
#     computes the flux for the bound myosin equation
#     using the inverse operator for the velocity
#     Helmholtz equation.
#     '''
#     N = setup.numerical.N
#     
#     L = velocity.make_linear_difference_operator(setup)
#     Dx = velocity.make_rhs_operator(setup)
#     
#     Linv = sparse.linalg.inv(L)
#     
#     A = -Linv*Dx
#     
#     def F(m):
#         v_cell = A*m
#         out = myosinadvection.compute_flux_godunov(m, v_cell, setup)
#         return out
#         
# #         v_face = myosinadvection.cell_to_face(v_cell)
# #         
# #         # Compute upwind direction
# #         sign_v = np.sign(v_face)
# #         m_uw = np.where(sign_v >=0, m[0:N-1], m[1:N])
# #     
# #         # Compute fluxes over all N+1 faces, using no flux
# #         # boundary conditions at edges
# #         out = np.zeros((N+1))
# #         out[1:N] = m_uw*v_face
# #         return out
#     
#     return F
# 
def create_myosin_function(setup, L = None, F = None):
    '''
    Create nonlinear function for the spatial discretization
    of the bound and freemyosin equation. Uses create_flux_function().
    '''
    N = setup.numerical.N
    h = geometry.get_h(N)
     
    if L is None:
        L = myosin.make_linear_difference_operator(setup)
 
    if F is None:
        F = create_flux_function(setup)
     
    def G(a):
        m = a[0:N]
        #n = a[N:]
         
        f = F(m)
         
        advection_term = (-1.0/h)*(f[1:N+1] - f[0:N])
         
         
        out = L*a
        out[0:N] += advection_term
         
        return out
     
    return G
 
def create_TR_function(setup, acurr, L = None, F = None):
    '''
    Create nonlinear function for solving differential equation
    using a Newton-Krylov method with a trapezoidal rule approximation
    to the myosin terms
    '''
    deltaT = setup.numerical.deltaT
     
    if L == None:
        L = myosin.make_linear_difference_operator(setup)
     
    if F == None:
        F = create_flux_function(setup)
         
    G = create_myosin_function(setup, L, F)
     
    def TR(a):
        return a - deltaT*G((a + acurr)/2) - acurr
     
    return TR
 
def iterate_TR(setup, a0, L = None, F = None, iterations = 1):
    if L == None:
        L = myosin.make_linear_difference_operator(setup)
     
    if F == None:
        F = create_flux_function(setup)
         
    a = a0
     
    for k in xrange(iterations):
        TR = create_TR_function(setup, a, L, F)
        aNew = optimize.newton_krylov(TR, a)
        a = aNew
     
    return a