'''
Created on Jan 31, 2014

@author: fogelson
'''

from .util import *
from . import myosin
from . import myosinadvection
from . import velocity

def iterateA(v0, m0, n0, setup, timestepfactor):
    """
    
    """
    v = velocity.solve(m0, setup)
    m = myosinadvection.iterate(m0, v, setup, timestepfactor=timestepfactor)
    return v, m, n0

def iterateB(v0, m0, n0, setup, timestepfactor):
    """
    
    """
    m, n = myosin.iterate(m0, n0, setup, timestepfactor=timestepfactor)
    return v0, m, n

def iterate(v0, m0, n0, setup, k=1):
    """
    
    """
    v, m, n = iterateA(v0, m0, n0, setup, 0.5)    
    v, m, n = iterateB(v, m, n, setup, 1.0)
    for i in xrange(k-1):
        v, m, n = iterateA(v, m, n, setup, 1.0)
        v, m, n = iterateB(v, m, n, setup, 1.0)
    v, m, n = iterateA(v, m, n, setup, 0.5)
    return v, m, n