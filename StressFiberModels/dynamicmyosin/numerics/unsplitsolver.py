'''
Created on May 16, 2016

@author: fogelson
'''

from .util import *
from . import myosin
from . import myosinadvection
from . import velocity


def iterate(v0, m0, n0, setup, k=1):
    """
    
    """
    for i in xrange(k):
        v = velocity.solve(m0, setup)
        m, n = myosin.iteratefull(m0, n0, v, setup)
        v = velocity.solve(m, setup)

    return v, m, n