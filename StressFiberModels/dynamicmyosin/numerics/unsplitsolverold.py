'''
Created on Feb 23, 2014

@author: fogelson
'''

from .util import *
from . import myosin
from . import myosinadvection
from . import velocity
from . import geometry
from scipy import optimize, sparse, interpolate
#from . import operator

def make_bdf_func(order, c):
    '''
    Makes a backward differentiation
    formula function of given order
    which uses the given coefficients.
    
    For example, to generate function
    called bdf1, this would be called as
    bdf2 = make_bdf_func(2, [1.0, -4.0/3.0, 1.0/3.0, -2.0/3.0]).
    
    The resulting function bdf2 would have the call
    signature bdf2(f, deltaT, y1, y0), and would solve the
    equation

        y2 - (4/3)*y1 + (1/3)*y0 - (2/3)*deltaT*f(y2)
        
    for y2.
    '''
    def bdf_func(f, deltaT, *yn):
        def func_to_solve(y):
            cyn = map(operator.mul, c[1:-1], yn)
            out = reduce(operator.add, cyn)
            out += c[0]*y + c[-1]*deltaT*f(y)
            return out
        return optimize.newton_krylov(func_to_solve, yn[0])
    
    return bdf_func

bdf1 = make_bdf_func(1, [1.0, -1.0, -1.0])
bdf2 = make_bdf_func(2, [1.0, -4.0/3.0, 1.0/3.0, -2.0/3.0])
bdf3 = make_bdf_func(3, [1.0, -18.0/11.0, 9.0/11.0, -2.0/11.0, -6.0/11.0])
bdf4 = make_bdf_func(4, [1.0, -48.0/25.0, 36.0/25.0, -16.0/25.0, 3.0/25.0, -12.0/25.0])
bdf5 = make_bdf_func(5, [1.0, -300.0/137.0, 300.0/137.0, -200.0/137.0, 75.0/137.0, -12.0/137.0, -60.0/137.0])

def iterate(m, n, setup, f = None):
    if f is None:
        f = make_full_func(setup)
    deltaT = setup.numerical.deltaT
    N = setup.numerical.N
    a = np.append(m, n)
    a = bdf1(f, deltaT, a)
    m = a[0:N]
    n = a[N:]
    return m, n

def make_myosin_func(setup):
    '''
    Returns a function f(a), where a is a column vector
    whose first N entries are m, and whose last N entries
    are n, which applies the spatial discretization to
    the myosin reaction diffusion equations to a
    '''
    A = myosin.make_linear_difference_operator(setup)
    def f(a):
        return A*a
    return f

def make_velocity_solver(setup):
    '''
    Returns a function v(m) that solves the discretized
    PDE for the velocity as a function of myosin distribution m.
    '''
    L = velocity.make_linear_difference_operator(setup)
    solveL = sparse.linalg.factorized(L)
    
    def v(m):
        rhs = velocity.make_rhs(m, setup)
        return solveL(rhs)
    
    return v

def make_myosin_advection_func(setup):
    '''
    Returns a function f(m), where m is the bound myosin distribution,
    that computes the spatial discretization to the advection term in
    the bound myosin equation.
    '''
    N = setup.numerical.N
    h = geometry.get_h(N)
    xCell = geometry.get_cell_centers(N)
    xFace = geometry.get_cell_edges(N)
    
    v = make_velocity_solver(setup)
    F = np.zeros_like(xFace)
    F_cell = np.zeros_like(xCell)
    
    def f(m):
        F_cell[:] = m*v
        interp = interpolate.UnivariateSpline(xCell, F_cell, s=0, k=3)
        F[1:-1] = interp(xFace[1:-1])
        
        #F = myosinadvection.compute_flux_godunov(m, v(m), setup)
        return -(1.0/h)*(F[1:N+1] - F[0:N])
    
    return f

def make_full_func(setup):
    '''
    Returns a function f(a), where a is a column vector made
    up of m and n, that computes the full spatial discretization
    to the PDEs for m and n.
    '''
    N = setup.numerical.N
    
    f_ad = make_myosin_advection_func(setup)
    f_rd = make_myosin_func(setup)
    
    def f(a):
        m = a[0:N]
        advection = f_ad(m)
        reactionDiffusion = f_rd(a)
        total = reactionDiffusion
        #total[0:N] += advection
        return total
    
    return f

def startup4(y0, f, setup, dtFrac = 8):
    deltaT = setup.numerical.deltaT/dtFrac
    
    yn = [y0]
    
    y1 = bdf1(f, deltaT, y0) # dt
    y2 = bdf2(f, deltaT, y1, y0) # 2dt
    y3 = bdf3(f, deltaT, y2, y1, y0) # 3dt
    y4 = bdf4(f, deltaT, y3, y2, y1, y0) # 4dt
    yn_startup = [y4, y3, y2, y1]
    if dtFrac == 4:
        yn.append(y4)
    
    for k in xrange(5, 1 + 3*dtFrac):
        ynew = bdf4(f, deltaT, *yn_startup)
        yn_startup = [ynew] + yn_startup[:-1]
        if k % dtFrac == 0:
            yn.append(ynew)
    yn.reverse()
    return yn

    

# 
# def iterate(v0, m0, n0, setup):
#     N = setup.numerical.N
#     deltaT = setup.numerical.deltaT
#     h = geometry.get_h(N)
#     
#     v = velocity.solve(m0, setup)
#     F = myosinadvection.compute_flux(m0, v, setup)
#     
#     A = myosin.make_linear_difference_operator(setup)
#     I = sparse.eye(2*N, 2*N)
#     LHS = I - (deltaT/2)*A
#     RHS = I + (deltaT/2)*A
#     
#     a0 = np.append(m0, n0)
#     fdiff = np.append((deltaT/h)*(F[1:N+1] - F[0:N]), np.zeros(n0.shape))
#     b = RHS*a0 - fdiff
#     
#     a = spla.spsolve(LHS, b)
#     m = a[0:N]
#     n = a[N:]
#     v = velocity.solve(m, setup)
#     
#     return v, m, n