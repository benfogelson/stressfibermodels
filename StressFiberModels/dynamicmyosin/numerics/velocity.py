'''
Created on Jan 27, 2014

@author: fogelson
'''

from .util import *
from . import geometry

def make_linear_difference_operator(setup):
    """
    Returns a sparse matrix L which applies the finite
    difference approximation to the PDE
    
    v''(x) - (alpha**2)*v + m' = 0
    v'(-1) + m(-1) =  beta*v(-1)
    v'(1)  + m(1)  = -beta*v(1)
    
    for evenly spaced, cell-centered values of v, as
    
    L*v = r,
    
    where r is the right hand side of system of finite
    difference equations, computed in make_rhs()
    """
    N = setup.numerical.N
    h = geometry.get_h(N)
    alpha = setup.physical.alpha
    if setup.physical.has_key('beta'):
        beta1 = setup.physical.beta
        beta2 = setup.physical.beta
    else:
        beta1 = setup.physical.beta1
        beta2 = setup.physical.beta2
    
    h2 = h**2
    
    lower = np.ones((N-1))/h2
    middle = -2*np.ones((N))/h2
    upper = np.ones((N-1))/h2
    L = sparse.diags([lower, middle, upper], [-1, 0, 1])
    
    L = L.tolil()
    
    L[0, :] = 0
    L[N-1, :] = 0

    L[0, 0] = -1.0/h2 - beta1*3/(2*h)
    L[0, 1] =  1.0/h2 + beta1/(2*h)
    
    L[N-1, N-2] = beta2/(2*h) + 1.0/h2
    L[N-1, N-1] = -beta2*3/(2*h) - 1.0/h2
    
    L = L - (alpha**2)*sparse.eye(N)
    
    return L

def make_rhs_operator(setup):
    """
    Linear operator for computing Dx * m
    """
    N = setup.numerical.N
    h = geometry.get_h(N)
    
    upper = np.ones((N-1))/(2*h)
    lower = -np.ones((N-1))/(2*h)
    Dx = sparse.diags([lower, upper], [-1, 1])
    Dx = Dx.tolil()
    Dx[0, :] = 0
    Dx[N-1, :] = 0
    Dx[0, 0] = 3.0/(2*h)         # 1.0/(2*h) old BCs
    Dx[0, 1] = -1.0/(2*h)        # 1.0/(2*h)
    Dx[N-1, N-1] = -3.0/(2*h)     # -1.0/(2*h)
    Dx[N-1, N-2] =  1.0/(2*h)    # -1.0/(2*h)
    
    return Dx

def make_rhs(m, setup):
    """
    Returns the right hand side to the system of linear
    finite difference equations computed in
    make_linear_difference_operator() for an array of
    values of m that are collocated with v at the cell
    centers.
    """
    Dx = make_rhs_operator(setup)
    
    return -Dx*m

def solve(m, setup):
    """
    Constructs the matrix and right hand side for the linear
    system of finite difference equations for v given a particular
    m, and solves the linear system, returning v.
    """
    L = make_linear_difference_operator(setup)
    rhs = make_rhs(m, setup)
    
    v = spla.spsolve(L, rhs)
    
    return v
