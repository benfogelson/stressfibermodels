import numpy as np
import NeuroTools.parameters as par
from sumatra.parameters import build_parameters
import datetime
import time
import os
import os.path
from dynamicmyosin.scripts.rununsplitsolver import Simulator
from dynamicmyosin.numerics import geometry
import logging
import pandas
import pickle
from multiprocessing import Manager
import matplotlib.pyplot as plt
import scipy.linalg as la
import scipy.interpolate as interp

# from dynamicmyosin.scripts import runsplitsolver


class Default(object):
    def __init__(self, baseparams, batchdataroot):
        self.baseparams = baseparams
        self.batchdataroot = batchdataroot

    def generator(self):
        params = par.ParameterSet(self.baseparams.pretty())
        params.batch.runlabel = '%d_%s' % (0, self.baseparams.sumatra_batch_label)
        yield params

    def report_run(self, runlabel, sim, *args, **kwargs):
        pass

    def report_batch(self, batchdataroot=None):
        pass

    def initialize(self, sim, params):
        logger = logging.getLogger(__name__)

        N = params.numerical.N
        h = geometry.get_h(N)
        randscale = params.batch.randscale

        sim.v[:] = 0
        sim.m[:] = 1 + randscale*(np.random.random(N) - 0.5)
        mtot = np.sum(h*sim.m)
        sim.m[:] /= mtot
        sim.n[:] = 0
        logger.debug('Initialized variables')

    def load(self, params):
        runlabel = str(params.batch.runlabel)
        logger = logging.getLogger(__name__)
        logger.debug('Started loading %s' % runlabel)

        savefolder = os.path.join(self.batchdataroot, runlabel)
        datafiles = os.listdir(savefolder)
        datafiles = filter(lambda s: s.endswith('.data'), datafiles)

        def compare(a, b):
            inta = int(a[:-5])
            intb = int(b[:-5])
            if inta < intb:
                return -1
            elif inta == intb:
                return 0
            else:
                return 1
        datafiles.sort(compare, reverse=True)

        with open(os.path.join(savefolder, datafiles[0]), 'r') as f:
            v = pickle.load(f)
            m = pickle.load(f)

        sim = Simulator(params, savefolder)
        sim.v = v
        sim.m = m
        sim.m[:] = 0
        sim.current_iteration = int(datafiles[0][:-5])
        logger.debug('Loaded data from iteration %d for run %s' % (sim.current_iteration, runlabel))

        self.report_run(runlabel, sim)
        logger.debug('Called run_report() for run %s' % runlabel)

        logger.debug('Finished loading %s' % runlabel)

    def run(self, params):
        runlabel = str(params.batch.runlabel)
        logger = logging.getLogger(__name__)
        logger.info('Started run %s' % runlabel)

        savefolder = os.path.join(self.batchdataroot, runlabel)
        paramfile = os.path.join(savefolder, '%s.params' % runlabel)

        if not os.path.exists(savefolder):
            os.makedirs(savefolder)
            logger.debug('Created data folder')

        # logger = logging.basicConfig(filename=os.path.join(savefolder, '%s.log' % runlabel), level=logging.DEBUG)
        start = time.time()
        # logging.debug('Started run %s' % (runlabel))

        with open(paramfile, 'w') as f:
            f.write(params.pretty())
            logger.debug('Saved parameter file')

        sim = Simulator(params, savefolder)
        logger.debug('Created Simulator() instance')

        self.initialize(sim, params)

        logger.debug('Started sim.run()')
        sim.run()
        logger.debug('Finished sim.run()')
        logger.info('Finished run %s' % runlabel)

        self.report_run(runlabel, sim)


class TemporalConvergenceStudy(Default):
    def __init__(self, baseparams, batchdataroot):
        Default.__init__(self, baseparams, batchdataroot)

        self.report = Manager().dict()

    def report_run(self, runlabel, sim, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug('Called report_run()')
        params = sim.setup

        report = {'sim': sim,
                  'deltaT': params.numerical.deltaT}

        self.report.update({params.simulator.iterations: report})

    def report_batch(self, batchdataroot=None):
        logger = logging.getLogger(__name__)

        if batchdataroot is None:
            batchdataroot = self.batchdataroot
        reportfilename = os.path.join(batchdataroot, 'report.data')

        self.report = dict(self.report)

        vnorms = []
        mnorms = []
        deltaTs = []

        for its0, its1 in zip(self.its_list[:-1], self.its_list[1:]):
            sim0 = self.report[its0]['sim']
            sim1 = self.report[its1]['sim']
            h0 = geometry.get_h(sim0.setup.numerical.N)
            h1 = geometry.get_h(sim1.setup.numerical.N)
            if h0 != h1:
                logger.error('Temporal convergence study requires constant h', exc_info=1)
                raise RuntimeError
            h = h0

            v0 = sim0.v
            m0 = sim0.m
            v1 = sim1.v
            m1 = sim1.m

            vnorm = h*la.norm((v1 - v0), 2)
            mnorm = h*la.norm((m1 - m0), 2)
            vnorms.append(vnorm)
            mnorms.append(mnorm)
            deltaTs.append(sim0.setup.numerical.deltaT)
        deltaTs = np.array(deltaTs)
        vnorms = np.array(vnorms)
        mnorms = np.array(mnorms)

        with open(reportfilename, 'w') as f:
            pickle.dump(deltaTs, f)
            pickle.dump(vnorms, f)
            pickle.dump(mnorms, f)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.loglog(deltaTs, vnorms, basex=2, basey=2)
        ax.loglog(deltaTs, mnorms, basex=2, basey=2)
        figfilename = os.path.join(self.batchdataroot, 'temporalconvergence_%s.png' % self.baseparams.sumatra_batch_label)
        fig.savefig(figfilename)

    def initialize(self, sim, params):
        logger = logging.getLogger(__name__)

        N = params.numerical.N
        h = geometry.get_h(N)
        x = geometry.get_cell_centers(N)

        bump = np.where(4.0*np.abs(x) < 1, np.exp(-1.0/(1.0 - (4.0*x)**2)), 0)
        const = 1.0

        sim.v[:] = 0
        sim.m[:] = bump + const
        mtot = np.sum(h*sim.m)
        sim.m[:] /= mtot
        sim.n[:] = 0
        logger.debug('Initialized variables')

    def generator(self):
        size = self.baseparams.batch.size
        deltaT_0 = self.baseparams.batch.deltaT_0
        its_0 = self.baseparams.batch.its_0
        block_size_0 = self.baseparams.batch.block_size_0

        powers = 2 ** np.arange(0, size)
        deltaT_list = list(deltaT_0/powers)
        its_list = list(its_0*powers)
        block_size_list = list(block_size_0*powers)

        # Keep this for report generation
        self.its_list = its_list

        for (idx, (its, deltaT, block_size)) in enumerate(zip(its_list, deltaT_list, block_size_list)):
            params = par.ParameterSet(self.baseparams.pretty())
            params.simulator.iterations = its
            params.simulator.block_size = block_size
            params.numerical.deltaT = deltaT

            params.batch.runlabel = \
                '%i_its_%d__deltaT_%03f__%s' % \
                (idx, its, deltaT, self.baseparams.sumatra_batch_label)

            yield params


class SpatialConvergenceStudy(Default):
    def __init__(self, baseparams, batchdataroot):
        Default.__init__(self, baseparams, batchdataroot)

        self.report = Manager().dict()

    def report_run(self, runlabel, sim, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug('Called report_run()')
        params = sim.setup

        report = {'sim': sim,
                  'h': geometry.get_h(params.numerical.N)}

        self.report.update({params.numerical.N: report})

    def report_batch(self, batchdataroot=None):
        logger = logging.getLogger(__name__)

        if batchdataroot is None:
            batchdataroot = self.batchdataroot
        reportfilename = os.path.join(batchdataroot, 'report.data')

        self.report = dict(self.report)

        vnorms = []
        mnorms = []
        hs = []

        for N0, N1 in zip(self.N_list[:-1], self.N_list[1:]):
            sim0 = self.report[N0]['sim']
            sim1 = self.report[N1]['sim']
            h0 = geometry.get_h(sim0.setup.numerical.N)
            h1 = geometry.get_h(sim1.setup.numerical.N)
            x0 = geometry.get_cell_centers(N0)
            x1 = geometry.get_cell_centers(N1)

            v0 = sim0.v
            m0 = sim0.m
            v1 = sim1.v
            m1 = sim1.m

            v1_0 = interp.interp1d(x1, v1)(x0)
            m1_0 = interp.interp1d(x1, m1)(x0)

            vnorm = h0*la.norm((v1_0 - v0), 2)
            mnorm = h0*la.norm((m1_0 - m0), 2)
            vnorms.append(vnorm)
            mnorms.append(mnorm)
            hs.append(h0)
        hs = np.array(hs)
        vnorms = np.array(vnorms)
        mnorms = np.array(mnorms)

        with open(reportfilename, 'w') as f:
            pickle.dump(hs, f)
            pickle.dump(vnorms, f)
            pickle.dump(mnorms, f)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.loglog(hs, vnorms, basex=2, basey=2)
        ax.loglog(hs, mnorms, basex=2, basey=2)
        figfilename = os.path.join(batchdataroot, 'spatialconvergence_%s.png' % self.baseparams.sumatra_batch_label)
        fig.savefig(figfilename)

    def initialize(self, sim, params):
        logger = logging.getLogger(__name__)

        N = params.numerical.N
        h = geometry.get_h(N)
        x = geometry.get_cell_centers(N)

        bump = np.where(4.0*np.abs(x) < 1, np.exp(-1.0/(1.0 - (4.0*x)**2)), 0)
        const = 1.0

        sim.v[:] = 0
        sim.m[:] = bump + const
        mtot = np.sum(h*sim.m)
        sim.m[:] /= mtot
        sim.n[:] = 0
        logger.debug('Initialized variables')

    def generator(self):
        size = self.baseparams.batch.size
        N_0 = self.baseparams.batch.N_0

        powers = 2 ** np.arange(0, size)
        N_list = list(N_0*powers)
        h_list = map(geometry.get_h, N_list)

        # Keep this for report generation
        self.N_list = N_list
        self.h_list = h_list

        for (idx, N) in enumerate(N_list):
            params = par.ParameterSet(self.baseparams.pretty())
            params.numerical.N = N

            params.batch.runlabel = \
                '%i_N_%d__%s' % \
                (idx, N, self.baseparams.sumatra_batch_label)

            yield params


class ScaleWithLength(Default):
    def __init__(self, baseparams, batchdataroot):
        Default.__init__(self, baseparams, batchdataroot)

        self.report = Manager().dict()

    def report_run(self, runlabel, sim, *args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug('Called report_run()')
        params = sim.setup

        N = params.numerical.N
        h = geometry.get_h(N)

        alpha = params.physical.alpha
        beta = params.physical.beta

        v = sim.v
        m = sim.m

        vl = 0.5*(3*v[0] - v[1])
        vr = 0.5*(3*v[-1] - v[-2])

        ml = 0.5*(3*m[0] - m[1])
        mr = 0.5*(3*m[-1] - m[-2])

        fdrag = h*np.sum((alpha**2)*v)

        fl = beta*vl + max(fdrag, 0)
        fr = -beta*vr + min(fdrag, 0)

        report = {'fl': fl,
                  'fr': fr,
                  'fdrag': fdrag,
                  'L': params.batch.L,
                  'alpha': alpha,
                  'beta': beta,
                  'final_iteration': sim.current_iteration}

        self.report.update({runlabel: report})

    def report_batch(self, batchdataroot=None):
        if batchdataroot is None:
            batchdataroot = self.batchdataroot
        reportfilename = os.path.join(batchdataroot, 'report.data')

        df = pandas.DataFrame.from_dict(dict(self.report), orient='index')
        df.sort_values('L', inplace=True)

        df.to_pickle(reportfilename)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(df.L.values, df.fl.values)

        figfilename = os.path.join(batchdataroot, 'forcescaling_%s.png' % self.baseparams.sumatra_batch_label)
        fig.savefig(figfilename)

    def generator(self):
        size = self.baseparams.batch.size

        L_0 = self.baseparams.batch.L_0
        L_final = self.baseparams.batch.L_final
        L_list = np.linspace(L_0, L_final, size)

        alpha_coeff = self.baseparams.batch.alpha_coeff
        beta_coeff = self.baseparams.batch.beta_coeff
        delta_coeff = self.baseparams.batch.delta_coeff

        alpha_list = [(alpha_coeff * L) for L in L_list]
        beta_list = [(beta_coeff * L) for L in L_list]
        delta_list = [(delta_coeff / (L**2)) for L in L_list]

        for (idx, (alpha, beta, delta, L)) in enumerate(zip(alpha_list, beta_list, delta_list, L_list)):
            params = par.ParameterSet(self.baseparams.pretty())
            params.physical.alpha = alpha
            params.physical.beta = beta
            params.physical.delta = delta
            params.physical.delta_small = delta

            params.batch.L = L

            params.batch.runlabel =\
                '%i_alpha_%03f__beta_%03f__delta_%03f__%s' %\
                (idx, alpha, beta, delta, self.baseparams.sumatra_batch_label)

            yield params


class Alpha(Default):
    def generator(self):
        for alpha in np.arange(0,6):
            params = par.ParameterSet(self.baseparams.pretty())
            params.physical.alpha = alpha

            params.batch.runlabel = int(alpha)

            yield params

    # configfilename = options[0]
    # label = options[1]
    # data_root = '/Users/fogelson/git/stressfibermodels/Data'
    # savefolder = os.path.join(data_root, label)
    # sim = Simulator(configfilename, savefolder)
    # sim.run()



# class DefaultRunner(object):
#     def __init__(self, params):
#         self.params = params
#
#     def __enter__(self):
#         if options is None:
#             options = sys.argv[1:]
#         configfilename = options[0]
#         label = options[1]
#         data_root = '/Users/fogelson/git/stressfibermodels/Data'
#         savefolder = os.path.join(data_root, label)
#         sim = Simulator(configfilename, savefolder)
#         sim.run()
#     def __exit__(self, exception_type, exception_value, traceback):
#         pass
#
# def base(baseparams):
#     yield baseparams
#
#
# def alphagen(baseparams):
#     for alpha in np.arange(0, 5):
#         params = par.ParameterSet(baseparams.pretty())
#         params.physical.alpha = alpha
#         yield params
