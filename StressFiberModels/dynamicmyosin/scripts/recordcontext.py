import contextlib
import os.path
import time
import traceback


@contextlib.contextmanager
def make_record(project, *args, **kwargs):
    """
    Context manager for creating a Sumatra record, ensuring
    that the record will be added to the project database
    even in the event of an error partway through execution.

    :param project: Sumatra project
    :param args: args passed to project.new_record(*args, **kwargs)
    :param kwargs: kwargs passed to project.new_record(*args, **kwargs)
    """
    start = time.time()
    rec = project.new_record(*args, **kwargs)
    rec.stdout_stderr = ''
    dataroot = os.path.join(project.data_store.root, rec.label)
    if not os.path.exists(dataroot):
        os.makedirs(dataroot)
    try:
        yield rec
    finally:
        stop = time.time()
        rec.duration = stop - start
        rec.output_data = rec.datastore.find_new_data(rec.timestamp)
        project.add_record(rec)
        project.save()
