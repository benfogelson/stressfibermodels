'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
from dynamicmyosin.numerics import geometry, unsplitsolver
from simulation import TimeDependentSimulator
import numpy as np
import numpy.linalg as la

import sys
import os
import os.path
import pickle
import logging

# import logging
# logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)

# import ConfigParser
import makemovie

from dynamicmyosin.numerics import initialize

import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt

class Simulator(TimeDependentSimulator):
    
    def doIsConverged(self):
        if not self.setup.simulator.convergence.on:
            return False
        order = self.setup.simulator.convergence.order
        tol = self.setup.simulator.convergence.tol
        if self.current_iteration == 0:
            return False
        else:
            vNorm = la.norm(self.v - self.vOld, order)
            mNorm = la.norm(self.m - self.mOld, order)
            nNorm = la.norm(self.n - self.nOld, order)
            norm = max(vNorm, mNorm, nNorm)
            if norm <= tol:
                return True
            else:
                return False
            
    
    def doInit(self, configfilename, savefolder):
        self.savefolder = savefolder

        x = dm.geometry.get_cell_centers(self.setup.numerical.N)
        self.x = x

        initializer = initialize.Initializer(self.setup)
        self.v = initializer.get_v()
        self.m = initializer.get_m()
        self.n = initializer.get_n()

    def initialize_variable(self, name):
        x = self.x
        try:
            val = self.__getattribute__(name)
            if type(val) == str:
                val = eval(val)
            elif type(val) == float or type(val) == int:
                val = val*np.ones(self.x.shape)
            self.__setattr__(name, val)
        except AttributeError:
            self.__setattr__(name, 0.0)
            self.initialize_variable(name)
    def doIterate(self, k):
        self.vOld, self.mOld, self.nOld = self.v, self.m, self.n
        self.v, self.m, self.n = unsplitsolver.iterate(self.v, self.m, self.n, self.setup, k)
    def save(self):
        logger = logging.getLogger(__name__)

        filename = os.path.join(self.savefolder, str(self.current_iteration) + '.data')

        N = self.setup.numerical.N
        h = geometry.get_h(N)
        if self.current_iteration == 0:
            logger.debug('Computing mtot0')
            self.mtot0 = h * np.sum(self.m)
            logger.debug('Computed mtot0 = %f' % self.mtot0)
            self.conservation_warning_given = False


        if self.current_iteration > 0:
            logger.debug('Iteration %d: Computing norms' % self.current_iteration)
            normFilename = os.path.join(self.savefolder, 'norms.txt')
            vNorm = la.norm(self.v - self.vOld, self.setup.simulator.convergence.order)
            mNorm = la.norm(self.m - self.mOld, self.setup.simulator.convergence.order)
            nNorm = la.norm(self.n - self.nOld, self.setup.simulator.convergence.order)
            norm = max(vNorm, mNorm, nNorm)
            logger.debug('Iteration %d: Norms computed' % self.current_iteration)
            with open(normFilename, 'a') as f:
                f.write('Iteration: ' + str(self.current_iteration))
                f.write('\t\tNorm: ' + str(norm))
                f.write('\n')
                f.flush()
            logger.debug('Iteration %d: Max norm written' % self.current_iteration)

        totalMyosinFilename = os.path.join(self.savefolder, 'totalmyosin.txt')
        N = self.setup.numerical.N
        h = geometry.get_h(N)
        mtot = h * np.sum(self.m)
        with open(totalMyosinFilename, 'a') as f:
            f.write('Iteration: ' + str(self.current_iteration))
            f.write('\t\tTotal myosin: ' + str(mtot))
            f.write('\n')
            f.flush()

        if not np.isclose([mtot], [self.mtot0]) and not self.conservation_warning_given:
            msg = 'Iteration %d: mtot = %f, but mtot0 = %f' % (self.current_iteration, mtot, self.mtot0)
            msg += '\nTotal myosin is not properly conserved. No future warnings will be given for this run.'
            logger.warning(msg)
            self.conservation_warning_given = True

        with open(filename, 'wb') as f:
            pickle.dump(self.v, f)
            pickle.dump(self.m, f)
            pickle.dump(self.n, f)

        if (self.setup.output.figs or
                (self.setup.output.finalfig and
                     (self.isConverged() or self.current_iteration >= self.setup.simulator.iterations))):
            # logger.debug('Starting to save fig')
            fig = plt.figure()
            # logger.debug('Created fig')
            ax = fig.add_subplot(111)
            ax.set_xlim(-0.5,0.5)
            # logger.debug('Created axis')
            vplot = ax.plot(self.x, self.v, label = "Actin velocity")
            # logger.debug('Plotted v')
            mplot = ax.plot(self.x, self.m, label = "Bound myosin concentration")
            nplot = ax.plot(self.x, self.n, label = "Unbound myosin concentration")
            # logger.debug('Plotted m')
            # logger.debug('Plotted n')
            # ax.legend()
            # logger.debug('Made legend')
            figfilename = os.path.join(self.savefolder, str(self.current_iteration) + '.png')
            # logger.debug('Planning to save to %s' % figfilename)
            fig.savefig(figfilename)
            # logger.debug('Saved to %s' % figfilename)
            plt.close(fig)
            # logger.debug('Figure closed')

def main(options = None):
    if options is None:
        options = sys.argv[1:]
    configfilename = options[0]
    label = options[1]
    data_root = '/Users/fogelson/git/stressfibermodels/Data'
    savefolder = os.path.join(data_root, label)
    sim = Simulator(configfilename, savefolder)
    sim.run()
#     # Load parameters
#     options = sys.argv
#     config_filename = options[1]
#     config = ConfigParser.SafeConfigParser()
#     config.read(config_filename)
#     iterations = config.getint("Simulation", "iterations")
#     save_freq = config.getint("Output", "save_freq")
#     
#     # Pass parameters to dynamic myosin package
#     dm.setup.loadparams(config_filename)
#     
#     # Set save location
#     label = options[-1]
#     data_root = 'Data'
#     save_folder = os.path.join(data_root, label)
#     
#     # Initialize
#     x = dm.setup.get_cell_centers(dm.setup.N)
#     v = np.zeros(x.shape)
#     m = np.ones(x.shape)
#     n = np.zeros(x.shape)
#     
#     # Pick functions for saving data and creating graphs
#     # Run solver, saving data along the way
#     
#     # Save initial values
#     saveIt(save_folder, 0, v, m, n)
#     
#     # Number of iteration blocks without saving
#     itBlocks = int(np.floor(float(iterations)/save_freq))
#     for b in xrange(1,itBlocks+1):
#         v, m, n = dm.splitsolver.iterate(v, m, n, save_freq)
#         saveIt(save_folder, b*save_freq, v, m, n)
#     # Do final few iterations if save_freq does not cleanly divide iterations
#     itFinal = iterations - itBlocks*save_freq
#     if itFinal > 0:
#         v, m, n = dm.splitsolver.iterate(v, m, n, itFinal)
#         saveIt(save_folder, iterations-1, v, m, n)
#     
#     # Save final data

if __name__ == '__main__':
    main()