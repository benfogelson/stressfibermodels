# from __future__ import print_function

# This should be first to guarantee a non-interactive backend
# otherwise matplotlib my freeze silently and hang subprocesses
# the first time we go to create a figure
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt
import seaborn

from dynamicmyosin.scripts import paramgenerators as pg
from dynamicmyosin.scripts.recordcontext import make_record
from dynamicmyosin.logger import QueueLogHandler

import click
import dynamicmyosin as dm
import importlib
import sumatra as smt
import NeuroTools.parameters as par
import multiprocessing as mp
import parameters
from sumatra.projects import load_project
from sumatra.parameters import build_parameters
import time
import datetime
import numpy as np
import os.path
import sys
import itertools
import logging
import logging.handlers


# def predebug(logger, msg, *args, **kwargs):
#     if logger.isEnabledFor(logging.DEBUG):
#         return prelog(logger, logging.DEBUG, msg, args, **kwargs)
#
#
# def preinfo(logger, msg, *args, **kwargs):
#     if logger.isEnabledFor(logging.INFO):
#         return prelog(logger, logging.INFO, msg, args, **kwargs)
#
#
# def prewarning(logger, msg, *args, **kwargs):
#     if logger.isEnabledFor(logging.WARNING):
#         return prelog(logger, logging.WARNING, msg, args, **kwargs)
#
#
# def preerror(logger, msg, *args, **kwargs):
#     if logger.isEnabledFor(logging.ERROR):
#         return prelog(logger, logging.ERROR, msg, args, **kwargs)
#
#
# def precritical(logger, msg, *args, **kwargs):
#     if logger.isEnabledFor(logging.CRITICAL):
#         return prelog(logger, logging.CRITICAL, msg, args, **kwargs)
#
#
# def prelog(logger, level, msg, args, exc_info=None, extra=None):
#     """
#     Creates a LogRecord instance that can be saved for later handling once
#     log handlers are correctly set up.
#
#     Code copied from the logging.Logger._log method, minus calling handlers
#     """
#     def currentframe():
#         """Return the frame object for the caller's stack frame."""
#         try:
#             raise Exception
#         except:
#             return sys.exc_info()[2].tb_frame.f_back
#     _srcfile = os.path.normcase(currentframe.__code__.co_filename)
#     if _srcfile:
#         # IronPython doesn't track Python frames, so findCaller raises an
#         # exception on some versions of IronPython. We trap it here so that
#         # IronPython can use logging.
#         try:
#             fn, lno, func = logger.findCaller()
#         except ValueError:
#             fn, lno, func = "(unknown file)", 0, "(unknown function)"
#     else:
#         fn, lno, func = "(unknown file)", 0, "(unknown function)"
#     if exc_info:
#         if not isinstance(exc_info, tuple):
#             exc_info = sys.exc_info()
#     record = logger.makeRecord(logger.name, level, fn, lno, msg, args, exc_info, func, extra)
#     return logger, record


class Config(object):
    def __init__(self):
        self.project = None
        self.batchname = None
        self.lock = None
        self.pool = None
        self.batchrecord = None
        self.prelogs = []


pass_config = click.make_pass_decorator(Config, ensure=True)

_thisfile = __file__
if _thisfile.endswith('.pyc') or _thisfile.endswith('.pyo'):
    _thisfile = _thisfile[:-4]
    _thisfile = _thisfile + '.py'

@click.group()
@click.option('-s', '--smtroot', default='.', type=click.Path())
@pass_config
def cli(config, smtroot):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    # config.prelogs.append(predebug('Running: %s' % ' '.join(sys.argv)))
    rootname = __name__.split('.')[0]
    rootlogger = logging.getLogger(rootname)
    rootlogger.setLevel(logging.INFO)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    streamhandler = QueueLogHandler(logging.StreamHandler())
    streamhandler.setLevel(logging.INFO)
    streamformatter = logging.Formatter('%(name)s:%(levelname)s:%(message)s')
    streamhandler.setFormatter(streamformatter)
    rootlogger.addHandler(streamhandler)
    rootlogger.debug('Added stream handler to root logger')

    logger = logging.getLogger(__name__)

    logger.debug('Running: %s' % ' '.join(sys.argv))

    logger.debug('Loading Sumatra project')
    config.project = load_project(str(smtroot))
    logger.debug('Sumatra project loaded')


def test(params):
    time.sleep(np.random.rand())
    click.echo(params.physical.alpha)
    return -1


def do_run(runargs):
    params, batchclass = runargs
    return batchclass.run(params)


def setup_root_logger(logfile):
    logdir = os.path.dirname(logfile)
    if not os.path.exists(logdir):
        os.makedirs(logdir)

    rootname = __name__.split('.')[0]
    rootlogger = logging.getLogger(rootname)
    filehandler = QueueLogHandler(logging.FileHandler(logfile))
    filehandler.setLevel(logging.DEBUG)
    fileformatter = logging.Formatter('%(asctime)s\t%(name)s:%(levelname)s: %(message)s')
    filehandler.setFormatter(fileformatter)
    rootlogger.addHandler(filehandler)
    rootlogger.info('Created log file at %s' % logfile)


@cli.command()
@click.argument('baseparams', type=str)
@click.option('-n','--batchname', default='', type=str)
@click.option('-ts', '--appendtimestep', default=True, type=bool)
@click.option('-np', '--numproc', default=1, type=int)
@pass_config
def run(config, baseparams, batchname, appendtimestep, numproc):
    start = time.time()
    timestamp = datetime.datetime.fromtimestamp(start).strftime('%Y%m%d-%H%M%S')

    if batchname == '':
        batchname = 'batch_%s' % timestamp
    elif appendtimestep:
        batchname = '%s_%s' % (batchname, timestamp)

    config.batchname = batchname
    dataroot = os.path.join(str(config.project.data_store), config.batchname)

    if os.path.exists(dataroot):
        raise IOError('The folder %s already exists. Cannot create a new batch with that name.' %
                      dataroot)
    else:
        os.makedirs(dataroot)

    # rootname = __name__.split('.')[0]
    # rootlogger = logging.getLogger(rootname)
    # rootlogger.setLevel(logging.INFO)
    # logger = logging.getLogger(__name__)
    # logger.setLevel(logging.INFO)
    # logfile = os.path.join(dataroot, '%s.log' % config.batchname)
    # filehandler = QueueLogHandler(logging.FileHandler(logfile))
    # filehandler.setLevel(logging.DEBUG)
    # fileformatter = logging.Formatter('%(asctime)s\t%(name)s:%(levelname)s: %(message)s')
    # filehandler.setFormatter(fileformatter)
    # rootlogger.addHandler(filehandler)
    # rootlogger.debug('Added file handler to root logger')

    logfile = os.path.join(dataroot, '%s.log' % config.batchname)
    setup_root_logger(logfile)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    baseparams = build_parameters(baseparams)
    logger.debug('Built parameters')

    baseparams.update({"sumatra_batch_label": config.batchname})
    config.batchrecord = config.project.new_record(parameters = baseparams,
                                                   label = config.batchname,
                                                   main_file = _thisfile
                                                   )

    baseparams.update({"command_line": ' '.join(sys.argv)})

    logger.debug('Created new record')

    config.batchrecord.datastore.root = dataroot

    try:
        batchclassname = baseparams.batch.batchclass
        logger.debug('Set batchclass name to %s' % batchclassname)
    except AttributeError:
        batchclassname = 'Default'
        logger.info('baseparams.batch.batchclass not specified. Reverting to Default.')

    try:
        batchclass = getattr(pg, batchclassname)(baseparams, config.batchrecord.datastore.root)
        logger.debug("Initialized %s() instance" % batchclassname)
    except AttributeError:
        logger.debug('Unknown batchclass %s' % batchclassname)
        raise
    paramgenerator = batchclass.generator()
    rungenerator = itertools.izip(paramgenerator,
                                  itertools.repeat(batchclass))
    runs = [r for r in rungenerator]
    logger.debug('Created list of runs')

    config.lock = mp.Lock()
    config.pool = mp.Pool(numproc)

    logger.info('Starting runs')
    config.pool.map(do_run, runs)
    logger.info('Finished runs')

    logger.debug('Writing batch report')
    batchclass.report_batch()

    config.batchrecord.duration = time.time() - start
    config.batchrecord.datastore.find_new_data(config.batchrecord.timestamp)
    logger.debug('Found new data')

    config.project.add_record(config.batchrecord)
    logger.debug('Added record to project')
    config.project.save()
    logger.debug('Saved project')


@cli.command()
@click.argument('batchname', type=str)
@click.option('-np', '--numproc', default=1, type=int)
@pass_config
def process(config, batchname, numproc):
    start = time.time()
    timestamp = datetime.datetime.fromtimestamp(start).strftime('%Y%m%d-%H%M%S')
    processlabel = 'processbatch__%s__%s' % (batchname, timestamp)
    logfile = os.path.join(str(config.project.data_store), processlabel, '%s.log' % processlabel)
    setup_root_logger(logfile)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # rootname = __name__.split('.')[0]
    # rootlogger = logging.getLogger(rootname)
    # filehandler = QueueLogHandler(logging.FileHandler(logfile))
    # filehandler.setLevel(logging.DEBUG)
    # fileformatter = logging.Formatter('%(asctime)s\t%(name)s:%(levelname)s: %(message)s')
    # streamformatter = logging.Formatter('%(name)s:%(levelname)s:%(message)s')
    # filehandler.setFormatter(fileformatter)

    if numproc != 1:
        logger.warning('Have not implemented multiprocessing support for the process subcommand yet')

    project = config.project

    try:
        batchrec = project.get_record(batchname)
        batchdataroot = batchrec.datastore.root
        baseparams = build_parameters(batchrec.parameters)
    except KeyError as err:
        logger.warning('Invalid record name %s' % batchname)
        batchdataroot = os.path.join(str(config.project.data_store), batchname)
        if os.path.exists(batchdataroot):
            print os.listdir().sort()
        sys.exit(1)

    baseparams = build_parameters(batchrec.parameters)

    try:
        batchclassname = baseparams.batch.batchclass
    except AttributeError as err:
        logger.error('Record parameter file does not contain batch.batchclass value')
        sys.exit(1)

    try:
        BatchClass = getattr(pg, batchclassname)
    except AttributeError as err:
        logger.error('Unknown batchclass %s' % batchclassname, exc_info=True)
        sys.exit(1)

    batchdataroot = batchrec.datastore.root
    batchclass = BatchClass(baseparams, batchdataroot)
    logger.debug('Initialized batchclass instance')

    logger.debug('Creating new Sumatra record for processing results')
    processparams = par.ParameterSet(baseparams)
    processparams.update({"command_line": ' '.join(sys.argv)})

    with make_record(project, parameters=processparams, label=processlabel, main_file=_thisfile) as processrec:
        logger.debug('Created Sumatra record')
        processdataroot = os.path.join(project.data_store.root, processlabel)
        processrec.datastore.root = processdataroot
        logger.debug('Set record root to %s' % processdataroot)

        sys.exit(0)

        def run_exists(params):
            runroot = os.path.join(batchdataroot, params.batch.runlabel)
            return os.path.exists(runroot)
        paramgenerator = itertools.ifilter(run_exists, batchclass.generator())
        logger.debug('Created param generator object')

        for params in paramgenerator:
            batchclass.load(params)
        logger.debug('Called batchclass.load() for findable runs in the batch')

        batchclass.report_batch(processdataroot)
        logger.debug('Generated batch report in %s' % processdataroot)


@cli.command()
@pass_config
def contexttest(config):
    start = time.time()
    timestamp = datetime.datetime.fromtimestamp(start).strftime('%Y%m%d-%H%M%S')
    label = 'contexttest__%s' % timestamp
    with make_record(config.project, parameters=None, label=label, main_file=_thisfile) as rec:
        raise NotImplementedError('You are a giant butt')


@cli.command()
@pass_config
def reduce():
    pass

if __name__=='__main__':
    run()
