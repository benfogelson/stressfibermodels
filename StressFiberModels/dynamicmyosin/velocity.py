'''
Created on Jan 27, 2014

@author: fogelson
'''

import setup
import numpy as np
from scipy import sparse
import scipy.sparse.linalg as spla

def make_linear_difference_operator():
    """
    Returns a sparse matrix L which applies the finite
    difference approximation to the PDE
    
    v''(x) - alpha*v + m' = 0
    v'(-1) + m(-1) =  beta*v(-1)
    v'(1)  + m(1)  = -beta*v(1)
    
    for evenly spaced, cell-centered values of v, as
    
    L*v = r,
    
    where r is the right hand side of system of finite
    difference equations, computed in make_rhs()
    """
    N = setup.N
    h = setup.get_h(N)
    alpha = setup.alpha
    beta = setup.beta
    
    h2 = h**2
    
    lower = np.ones((N-1))/h2
    middle = -2*np.ones((N))/h2
    upper = np.ones((N-1))/h2
    L = sparse.diags([lower, middle, upper], [-1, 0, 1])
    
    L = L.tolil()
    
    L[0,:] = 0
    L[N-1,:] = 0

    L[0,0] = -1.0/h2 - beta*3/(2*h)
    L[0,1] =  1.0/h2 + beta/(2*h)
    
    L[N-1,N-2] = beta/(2*h) + 1.0/h2
    L[N-1,N-1] = -beta*3/(2*h) - 1.0/h2
    
    L = L - alpha*sparse.eye(N)
    
    return L

def make_rhs(m):
    """
    Returns the right hand side to the system of linear
    finite difference equations computed in
    make_linear_difference_operator() for an array of
    values of m that are collocated with v at the cell
    centers.
    """
    N = setup.N
    h = setup.get_h(N)
    
    rhs = np.zeros((N))
    I = np.arange(1,N-1) # All indices from 1 to N-2
    rhs[I] = (m[I+1] - m[I-1])/(2*h)
    
    rhs[0] = (m[1] + m[0])/(2*h)
    rhs[N-1] = -(m[N-1] + m[N-2])/(2*h)
    
    return -rhs

def solve(m):
    """
    Constructs the matrix and right hand side for the linear
    system of finite difference equations for v given a particular
    m, and solves the linear system, returning v.
    """
    L = make_linear_difference_operator()
    rhs = make_rhs(m)
    
    v = spla.spsolve(L, rhs)
    
    return v