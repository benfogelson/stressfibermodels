'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
import NeuroTools.parameters as par

from sumatra.parameters import build_parameters
from sumatra.projects import load_project
import time

import numpy as np
import os.path
import sys
import pickle
import matplotlib.pyplot as plt
from matplotlib import animation

def make_force_plot(setup, datafolder, savefolder):
    plt.ioff()
    
    iterations = setup.simulator.iterations
    block_size = setup.simulator.block_size
    N = setup.numerical.N
    x = dm.geometry.get_cell_centers(N)
    beta1 = setup.physical.beta1
    beta2 = setup.physical.beta2

    paths = []
    times = []
     
    for k in xrange(iterations+1):
        if k % block_size == 0 or k == iterations:
            path = os.path.join(datafolder, str(k) + '.data')
            if os.path.exists(path):
                paths.append(path)
                times.append(k)
    
    times = np.array(map(lambda k: setup.numerical.deltaT*k, times))

    f_left = []
    f_right = []

    for path in paths:
        with open(path, 'rb') as f:
            v = pickle.load(f)
            vl = (3*v[0] - v[1])/2
            vr = (3*v[-1] - v[-2])/2
            f_left.append(beta1*vl)
            f_right.append(-beta2*vr)
    
    f_left = np.array(f_left)
    f_right = np.array(f_right)
    
    f_diff = f_right - f_left
    
    fig = plt.figure()
    ax_left = fig.add_subplot(311)
    ax_right = fig.add_subplot(312)
    ax_diff = fig.add_subplot(313)
     
    ax_left.plot(times, f_left, color='blue', linewidth=2)
    ax_left.set_title('Force at left endpoint')
     
    ax_right.plot(times, f_right, color='blue', linewidth=2)
    ax_right.set_title('Force at right endpoint')
     
    ax_diff.plot(times, f_diff, color='red', linewidth=2)
    ax_diff.set_title('right - left')
     
    figname = 'forces.png'
    figpath = os.path.join(savefolder, figname)
#     
#     plt.draw()
#     
    fig.savefig(figpath)
    plt.close(fig)
    

def main():
    project = load_project()
    options = sys.argv
    
    if len(options) == 1:
        simLabel = project.most_recent()
    elif len(options) == 2:
        simLabel = options[1]
        
    simRecord = project.get_record(simLabel)
        
    setup = par.ParameterSet(simRecord.parameters.as_dict())
    
    simLabel = simRecord.label
    outLabel = simRecord.label + '.force'
    outRecord = project.new_record(main_file = __file__, label=outLabel)
    outRecord.datastore.root = os.path.join(outRecord.datastore.root, outRecord.label)
    
    datafolder = simRecord.datastore.root
    savefolder = outRecord.datastore.root
    
    start_time = time.time()
    
    make_force_plot(setup, datafolder, savefolder)
    
    outRecord.duration = time.time() - start_time
    outRecord.output_data = outRecord.datastore.find_new_data(outRecord.timestamp)
    project.add_record(outRecord)
    project.save()
    
if __name__ == '__main__':
    main()