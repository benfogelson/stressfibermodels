'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
import NeuroTools.parameters as par

from sumatra.parameters import build_parameters
from sumatra.projects import load_project
import time

import numpy as np
import os.path
import sys
import pickle
import matplotlib.pyplot as plt
from matplotlib import animation

def make_movie(setup, datafolder, savefolder, moviename):
    iterations = setup.simulator.iterations
    block_size = setup.simulator.block_size
    N = setup.numerical.N
    x = dm.geometry.get_cell_centers(N)

    paths = []
     
    for k in xrange(iterations+1):
        if k % block_size == 0 or k == iterations:
            path = os.path.join(datafolder, str(k) + '.data')
            if os.path.exists(path):
                paths.append(path)
    
    # Read through all the data once to find min and max values for v, m, and n
    vmin = np.inf
    vmax = -np.inf
    mmin = np.inf
    mmax = -np.inf
    nmin = np.inf
    nmax = -np.inf
     
    def update_min_max(oldMin, oldMax, y):
        newMin = min(oldMin, y.min())
        newMax = max(oldMax, y.max())
        return newMin, newMax
     
    for path in paths:
        with open(path, 'rb') as f:
            v = pickle.load(f)
            m = pickle.load(f)
            n = pickle.load(f)
            vmin, vmax = update_min_max(vmin, vmax, v)
            mmin, mmax = update_min_max(mmin, mmax, m)
            nmin, nmax = update_min_max(nmin, nmax, n)
    
    
    myomin = min(mmin, nmin)
    myomax = max(mmax, nmax)
     
    # Set up figure and axes
    fig = plt.figure()
    
    vAxis = fig.add_subplot(311)
    mAxis = fig.add_subplot(312)
    nAxis = fig.add_subplot(313)
    
    vAxis.set_xlim(x.min(), x.max())
    vAxis.set_ylim(vmin, vmax)
    vLine, = vAxis.plot([], [], linewidth=2, color='blue')
     
    mAxis.set_xlim(x.min(), x.max())
    mAxis.set_ylim(myomin, myomax)
    mLine, = mAxis.plot([], [], linewidth=2, color='red')
     
    nAxis.set_xlim(x.min(), x.max())
    nAxis.set_ylim(myomin, myomax)
    nLine, = nAxis.plot([], [], linewidth=2, color='orange')
    
    # init and animate functions
    def init_frame():
        vLine.set_data([], [])
        mLine.set_data([], [])
        nLine.set_data([], [])
        return vLine, mLine, nLine
     
    def animate(i):
        path = paths[i]
        with open(path, 'rb') as f:
            v = pickle.load(f)
            m = pickle.load(f)
            n = pickle.load(f)
        vLine.set_data(x, v)
        mLine.set_data(x, m)
        nLine.set_data(x, n)
        return vLine, mLine, nLine
    
    # Call animator
    anim = animation.FuncAnimation(fig, animate, init_func=init_frame, frames=len(paths), interval=20, blit=False)
     
    anim.save(os.path.join(savefolder, moviename + '.mp4'), fps=30, extra_args=['-vcodec', 'libx264'])
    

def main():
    project = load_project()
    options = sys.argv
    
    if len(options) == 1:
        simLabel = project.most_recent()
    elif len(options) == 2:
        simLabel = options[1]
        
    simRecord = project.get_record(simLabel)
        
    setup = par.ParameterSet(simRecord.parameters.as_dict())
    
    simLabel = simRecord.label
    movLabel = simRecord.label + '.movie'
    movRecord = project.new_record(main_file = __file__, label=movLabel)
    movRecord.datastore.root = os.path.join(movRecord.datastore.root, movLabel)
    
    datafolder = simRecord.datastore.root
    savefolder = movRecord.datastore.root
    moviename = simLabel
    
    start_time = time.time()
    
    make_movie(setup, datafolder, savefolder, moviename)
    
    movRecord.duration = time.time() - start_time
    movRecord.output_data = movRecord.datastore.find_new_data(movRecord.timestamp)
    project.add_record(movRecord)
    project.save()
    
if __name__ == '__main__':
    main()