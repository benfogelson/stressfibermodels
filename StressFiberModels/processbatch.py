'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
import NeuroTools.parameters as par

from sumatra.parameters import build_parameters
from sumatra.projects import load_project
import time

import numpy as np
import os.path
import sys
import pickle
import matplotlib.pyplot as plt
from matplotlib import animation


def process(project, savefolder):
    labels = project.record_store.labels(project.name)
    labelEnds = map(lambda l: l[17:], labels)
    
    alphas = np.linspace(0.01,10.0,20).tolist()
    betas = np.linspace(0.01,10.0,20).tolist()
    
    Na = len(alphas)
    Nb = len(betas)
    
    Fl = np.zeros((Na, Nb))
    Fr = np.zeros((Na, Nb))
    Alpha = np.zeros((Na, Nb))
    Beta = np.zeros((Na, Nb))
    
    for ia in xrange(Na):
        for ib in xrange(Nb):
            alpha = alphas[ia]
            beta = betas[ib]
            label = find_label(labels, labelEnds, alpha, beta)
            record = load_record(project, label)
            setup = load_setup(record)
            v, m, n = load_iteration(record, setup)
            v, m, n = flip_by_max_m(v, m, n)
            fl, fr = compute_force(setup, v)
            Fl[ia, ib] = fl
            Fr[ia, ib] = fr
            Alpha[ia, ib] = alpha
            Beta[ia, ib] = beta
    
    dataFileName = 'force_sweep_2_forces.data'
    dataFilePath = os.path.join(savefolder, dataFileName)
    with open(dataFilePath, 'wb') as f:
        pickle.dump(Alpha, f)
        pickle.dump(Beta, f)
        pickle.dump(Fl, f)
        pickle.dump(Fr, f)
    
#     figLeftFileName = 'force_sweep_1_forces_left.png'
#     figLeftFilePath = os.path.join(savefolder, figLeftFileName)
#     
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     ax.contourf(Alpha, Beta, Fl)
#     plt.colorbar()
#     fig.savefig(figLeftFilePath)
#     plt.close(fig)
#     
#     figRightFileName = 'force_sweep_1_forces_right.png'
#     figRightFilePath = os.path.join(savefolder, figRightFileName)
#     
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     ax.contourf(Alpha, Beta, Fr)
#     plt.colorbar()
#     fig.savefig(figRightFilePath)
#     plt.close(fig)
#     
#     figDiffFileName = 'force_sweep_1_forces_diff.png'
#     figDiffFilePath = os.path.join(savefolder, figDiffFileName)
#     
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     ax.contourf(Alpha, Beta, Fr - Fl)
#     plt.colorbar()
#     fig.savefig(figDiffFilePath)
#     plt.close(fig)

def make_label_end(alpha, beta):
    labelEnd = ''
    labelEnd += 'physical.alpha_'
    labelEnd += str(alpha)
    labelEnd += '__physical.beta_'
    labelEnd += str(beta)
    labelEnd += '__force_sweep_2'
    return labelEnd

def find_label(labels, labelEnds, alpha, beta):
    labelEnd = make_label_end(alpha, beta)
    i = labelEnds.index(labelEnd)
    label = labels[i]
    return label
    
def load_record(project, label):
    record = project.get_record(label)
    return record

def load_setup(record):
    setup = par.ParameterSet(record.parameters.as_dict())
    return setup

def get_last_iteration(record):
    normsPath = os.path.join(record.datastore.root, 'norms.txt')
    with open(normsPath, 'r') as f:
        lines = f.readlines()
    lastLine = lines[-1]
    lastIteration = int(lastLine.split()[1])
    return lastIteration

def load_iteration(record, setup, iteration=None):
    if iteration is None or iteration == -1:
        iteration = get_last_iteration(record)
    dataFileName = str(iteration) + '.data'
    dataFilePath = os.path.join(record.datastore.root, dataFileName)
    with open(dataFilePath, 'rb') as f:
        v = pickle.load(f)
        m = pickle.load(f)
        n = pickle.load(f)
    return v, m , n

def flip_by_max_m(v, m, n):
    m_ind = np.nonzero(m == m.max())[0]
    N = len(m)
    
    if m_ind >= N/2.:
        flip = lambda A: np.flipud(A)
        v = -v
    else:
        flip = lambda A: A
    v, m, n = map(flip, (v, m, n))
    return v, m, n

def compute_endpoints(A):
    Al = (3*A[0] - A[1])/2
    Ar = (3*A[-1] - A[-2])/2
    
    return Al, Ar

def compute_force(setup, v):
    vl, vr = compute_endpoints(v)
    
    if setup.physical.has_key('beta'):
        beta1 = setup.physical.beta
        beta2 = setup.physical.beta
    else:
        beta1 = setup.physical.beta1
        beta2 = setup.physical.beta2
        
    fl = beta1*vl
    fr = -beta2*vr
    
    return fl, fr

def main():
    project = load_project()
    
    record = project.new_record(main_file = __file__)
    record.datastore.root = os.path.join(record.datastore.root, record.label)
    savefolder = record.datastore.root
    
    start_time = time.time()
    
    process(project, savefolder)
    
    record.duration = time.time() - start_time
    record.output_data = record.datastore.find_new_data(record.timestamp)
    project.add_record(record)
    project.save()
    
if __name__ == '__main__':
    main()