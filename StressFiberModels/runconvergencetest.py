'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
import NeuroTools.parameters as par

import numpy as np
import os.path
import sys
import pickle
import matplotlib.pyplot as plt


def bump(x):
    return np.where(abs(x) < 1, np.exp(-1.0/(1 - x**2)), 0.0)

def solveAdvection(setup):
    N = setup.numerical.N
    x = dm.geometry.get_cell_centers(N)
    k = setup.simulator.iterations
    
    v = np.ones_like(x)
    m = bump(4*x)
    
    for i in xrange(k):
        m = dm.myosinadvection.iterate(m, v, setup)
        
    sol = dm.convergence.Solution(x, m)
    
    return sol

def solveReactionDiffusion(setup):
    N = setup.numerical.N
    x = dm.geometry.get_cell_centers(N)
    k = setup.simulator.iterations
    
    m = bump(4*x)
    n = np.zeros_like(x)
    
    A = dm.myosin.make_linear_difference_operator(setup)
    for i in xrange(k):
        m, n = dm.myosin.iterate(m, n, setup, A)
    
    sol = dm.convergence.Solution(x, (m, n))
    
    return sol

def solveSplit(setup):
    N = setup.numerical.N
    x = dm.geometry.get_cell_centers(N)
    k = setup.simulator.iterations
    
    v = np.zeros_like(x)
    m = np.ones_like(x)
    n = np.zeros_like(x)
    
    v, m, n = dm.splitsolver.iterate(v, m, n, setup, k)
    
    sol = dm.convergence.Solution(x, (v, m, n))
    
    return sol

def save_output(savefolder, setup, tester, solVarNames):
    if setup.convergence.has_key('order'):
        order = setup.convergence.order
    else:
        order = 1.0
    
    if type(solVarNames) == str:
        solVarNames = [solVarNames]
    numVars = len(solVarNames)
    if numVars == 1:
        solList = [tester.sols]
    else:
        solList = zip(*map(lambda s: (map(lambda y: dm.convergence.Solution(s.x, y), s.y)), tester.sols))
    
    reports = map(lambda sols, name: make_report(sols, name, order), solList, solVarNames)
    report = reduce(lambda x, y: x + '\n' + y, reports)
    
    reportfile = os.path.join(savefolder, 'report.txt')
    with open(reportfile, 'w') as f:
        f.write(report)
        f.flush()
    
    testerfile = os.path.join(savefolder, 'tester.data')
    with open(testerfile, 'wb') as f:
        pickle.dump(tester, f)
    
    for sols, name in zip(solList, solVarNames):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for sol in sols:
            ax.plot(sol.x, sol.y)
        fig.savefig(os.path.join(savefolder, name + '.png'))

def make_report(sols, varname, order):
    '''
    String reporting the convergence results given
    in ratios for variable with name varname
    '''
    ratios = dm.convergence.error_ratios(sols, order)
    errs   = map(lambda sol1, sol2: dm.convergence.compute_error(sol1, sol2, order), sols[0:-1], sols[1:])
    
    ratio_report = 'Relative error ratios for subsequent runs for ' + varname + ': ' + str(ratios)
    error_report = 'Error in ' + str(order) + ' norm for ' + varname + ': ' + str(errs)
    
    return error_report + '\n' + ratio_report

def main():
    options = sys.argv
    configfilename = options[1]
    label = options[-1]
    data_root = '/Users/fogelson/git/stressfibermodels/Data'
    savefolder = os.path.join(data_root, label)
    
    setup = par.ParameterSet(configfilename)
    
    if setup.convergence.has_key('solver'):
        solverName = setup.convergence.solver
    else:
        solverName = 'split'
    
    if solverName == 'split':
        solveFunc = solveSplit
        varNames = ['v', 'm', 'n']
    elif solverName == 'reactiondiffusion':
        solveFunc = solveReactionDiffusion
        varNames = ['m', 'n']
    elif solverName == 'advection':
        solveFunc = solveAdvection
        varNames = ['m']
    
    tester = dm.convergence.ConvergenceTester(solveFunc, setup)
    
    tester.compute_all_solutions()
    
    save_output(savefolder, setup, tester, varNames)

if __name__ == '__main__':
    main()