import subprocess
import NeuroTools.parameters as par
import time
import datetime
import os.path
import multiprocessing as mp
from sumatra.projects import load_project
from sumatra.parameters import build_parameters, YAMLParameterSet
import makemovie
import makeforceplot
import numpy as np
import sys
import matplotlib.pyplot as plt
import pickle
import itertools


import runfixedmyosinsolver

def iter_flat(paramspace,copy=False):
    """
    Modified from NeuroTools.parametersParameterSpace.iter_inner_range_keys
    
    An iterator of the ParameterSpace paramspace which yields ParameterSets
    with flattened combinations of ParameterRange elements
    """
    
    keys = paramspace.range_keys()
    if len(keys)==0:
        # return an iterator over 1 copy for modifying
        yield paramspace.tree_copy()
        return
    
    flatIterator = itertools.izip(*itertools.imap(lambda k: paramspace[k], keys))
    
    
    if not copy:
        # Make one copy to modify
        tmp = paramspace.tree_copy()
        for valTuple in flatIterator:
            for key, val in zip(keys, valTuple):
                tmp[key] = val
            if not tmp._is_space():
                tmp = par.ParameterSet(tmp)
            yield tmp
    
    else:
        for valTuple in flatIterator:
            # Make a new copy at each tuple of parameter values
            tmp_copy = paramspace.tree_copy()
            for key, val in zip(keys, valTuple):
                tmp_copy[key] = val
            if not tmp_copy._is_space():
                tmp_copy = par.ParameterSet(tmp_copy)
            yield tmp_copy


project = load_project()

project

lock = None
pool = None


if __name__ == '__main__':
    options = sys.argv[1:]
    configfilename = options[0]
    params = par.ParameterSpace(configfilename)
else:
    params = par.ParameterSpace('/Users/fogelson/git/stressfibermodels/config.params')
    
for k in params.flatten().keys():
    v = params[k]
    if type(v) == str and v.lower().startswith('eval: '):
        params[k] = eval(params[k][5:])

physical = params.physical

outputConfigDir = '/Users/fogelson/git/stressfibermodels'

def make_label(setup, num):
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d-%H%M%S')
    label = timestamp + '__' + str(num) + '__'
    for name in params.range_keys():
        label += name + '_' + str(setup[name]) + '__'
    if label.endswith('__'):
        label = label[0:-2]
    label += '__' + batchname
    return label

def do_sweep((setup, num)):
    global lock
    global currentversion
    label = make_label(setup, num)
    outputConfigFilename = os.path.join(outputConfigDir, label+'.params')
    setup._url = outputConfigFilename
    setup.save(outputConfigFilename)
    
    with lock:
        rec = project.new_record(parameters = build_parameters(outputConfigFilename), label=label, main_file=__file__, version=currentversion)
    
    rec.datastore.root = os.path.join(rec.datastore.root, label)
    start_time = time.time()
    
    runfixedmyosinsolver.main([outputConfigFilename, label, data_root])
    
    rec.duration = time.time() - start_time
    rec.output_data = rec.datastore.find_new_data(rec.timestamp)
    
    with lock:
        project.add_record(rec)
        project.save()
       
    os.remove(outputConfigFilename)
       
    #os.remove(outputConfigFilename)
    
    #subprocess.call(['smt', 'run', outputConfigFilename, '--label='+label])
    #subprocess.call(['python', 'StressFiberModels/makemovie.py', label])
    #subprocess.call(['rm', outputConfigFilename])
    
    return label

# def setup_sweeps(names):
#     # Returns a list, each of whose entries is a dict
#     # corresponding to one simulation
#     if flatSweep:
#         out = []
#         sweepLength = len(sweeps[names[0]])
#         for i in xrange(sweepLength):
#             subout = {}
#             for name in names:
#                 subout[name] = sweeps[name][i]
#             out.append(subout)
#     else:
#         if len(names) == 1:
#             name = names[0]
#             vals = sweeps[name]
#             out = []
#             for v in vals:
#                 out.append({name: v})
#         elif len(names) > 1:
#             name = names[0]
#             vals = sweeps[name]
#             out = []
#             for v in vals:
#                 subout = setup_sweeps(names[1:])
#                 for d in subout:
#                     d[name] = v
#                 out += subout
#     return out

def sweep_through():
    if params.sweep.flatSweep:
        paramIterator = iter_flat(params,True)
    else:
        paramIterator = params.iter_inner(True)
    nums = itertools.count()
    
    numProc = params.sweep.numproc
    global lock
    global pool
    global movieLock
    
    lock = mp.Lock()
    pool = mp.Pool(numProc)
    
    labels = pool.map(do_sweep, itertools.izip(paramIterator, nums))
    #labels = map(do_sweep, itertools.izip(paramIterator, nums)) # Non parallel map useful for better traces in debugging
        
    if params.output.figs:
        for l in labels:
            runfolder = os.path.join(data_root, l)
            datafilename = os.path.join(runfolder, 'results.data')
            with open(datafilename, 'rb') as f:
                x = pickle.load(f)
                v = pickle.load(f)
                m = pickle.load(f)
            fig = plt.figure()
            v_ax = fig.add_subplot(211)
            m_ax = fig.add_subplot(212)
            v_ax.plot(x, v, color='blue', linewidth=3)
            m_ax.plot(x, m, color='red', linewidth=3)
            v_ax.set_xlabel(r'\(x\)')
            m_ax.set_xlabel(r'\(x\)')
            v_ax.set_ylabel(r'\(v\)')
            m_ax.set_ylabel(r'\(m\)')
            figfilename = os.path.join(savefolder, l + '.png')
            fig.savefig(figfilename)
            plt.close(fig)
    
    sweepNames = params.range_keys()
    forceDict = {}
    
    for name in sweepNames:
        forceDict[name] = np.zeros(len(labels))
        
    forceDict['fl'] = np.zeros(len(labels))
    forceDict['fr'] = np.zeros(len(labels))
    
    if params.sweep.flatSweep:
        paramIterator = iter_flat(params,True)
    else:
        paramIterator = params.iter_inner(True)
    nums = itertools.count()
    
    for setup, label, num in itertools.izip(paramIterator, labels, nums):
        runfolder = os.path.join(data_root, label)
        datafilename = os.path.join(runfolder, 'results.data')
        
        with open(datafilename, 'rb') as f:
            x = pickle.load(f)
            v = pickle.load(f)
            m = pickle.load(f)
            
        vl = (3*v[0] - v[1])/2
        vr = (3*v[-1] - v[-2])/2
        
        if setup.physical.has_key('beta'):
            beta1 = setup.physical.beta
            beta2 = setup.physical.beta
        else:
            beta1 = setup.physical.beta1
            beta2 = setup.physical.beta2
        
        forceDict['fl'][num] = beta1*vl
        forceDict['fr'][num] = -beta2*vr
        
        for name in params.range_keys():
            forceDict[name][num] = setup[name]
    
    forcefilename = os.path.join(savefolder, 'forces.data')
    with open(forcefilename, 'wb') as f:
        pickle.dump(forceDict, f)
    
    '''
    Plot force vs some x axis for a sweep, and if requested
    also plot average at each of those variables
    '''
    if params.sweep.force.plot:
        xvar = forceDict[params.sweep.force.xvar]
        fl = forceDict['fl']
        fr = forceDict['fr']
        forceFig = plt.figure()
        if params.sweep.force.plotaverage:
            rawForceAxis = forceFig.add_subplot(211)
        else:
            rawForceAxis = forceFig.add_subplot(111)
        rawForceAxis.plot(xvar, fl, 'bo')
        rawForceAxis.plot(xvar, fr, 'ro')
        
        if params.sweep.force.plotaverage:
            avgForceAxis = forceFig.add_subplot(212)
            xvarSet = set(xvar)
            for x in xvarSet:
                atVal = xvar == x
                flAvg = np.mean(fl[atVal])
                frAvg = np.mean(fr[atVal])
                avgForceAxis.plot(x, flAvg, 'bo')
                avgForceAxis.plot(x, frAvg, 'ro')
        
        forceFigFilename = os.path.join(savefolder, params.sweep.force.xvar + '_vs_force.png')
        forceFig.savefig(forceFigFilename)
    
    batchmemberfilename = os.path.join(savefolder, 'memberlabels')
    with open(batchmemberfilename, 'wb') as f:
        pickle.dump(labels, f)
        

if __name__ == '__main__':
    options = sys.argv[1:]
    configfilename = options[0]
    label = options[1]
    global data_root
    data_root = os.path.abspath(project.data_store.root)
    global savefolder
    savefolder = os.path.join(data_root, label)
    
    global currentversion
    currentversion = project.default_repository.get_working_copy().current_version()
    
    global batchname
    batchname = params.sweep.batchname
    sweep_through()