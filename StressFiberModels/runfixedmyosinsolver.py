'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
from simulation import TimeDependentSimulator, TimeIndependentSimulator
import numpy as np
import numpy.linalg as la
from sumatra.projects import load_project

import sys
import os
import os.path
import pickle
import ConfigParser

from dynamicmyosin.numerics import initialize, velocity

import matplotlib
import matplotlib.pyplot as plt

class MyosinGenerator:
    def generate(self, x, setup):
        '''
        Generate a distribution of myosin according to some prescribed
        process
        '''
        mode = setup.myosin.mode
        if mode == 'constant':
            c = setup.myosin.c
            m = c*np.ones(x.shape)
        elif mode == 'central_bump':
            height = setup.myosin.height
            width = setup.myosin.width
            base = setup.myosin.base
            y = x/(width/2.)
            m = base + (height-base)*np.where(np.abs(y) < 1., np.exp(1-1/(1-y**2)), 0)
        elif mode == 'random':
            np.random.seed()
            wavenumbers = np.arange(setup.myosin.maxwavenumber+1,dtype=np.float64)
            decay = setup.myosin.decay
            scalefactors = np.where(wavenumbers != 0, setup.myosin.amplitudescale/wavenumbers**decay, 1.)
            waveamplitudes = np.random.normal(0.0, 1.0, (len(wavenumbers),2))
            physicalinterval = np.float64(setup.myosin.physicalinterval) # How many um an interval of 2*pi corresponds to
            physicallength = np.float64(setup.myosin.physicallength)
            intervallength = 2*np.pi*physicallength/physicalinterval # Length of the (-pi, pi) interval over which the periodic distribution is generated to sample
            a = np.random.uniform(-np.pi, np.pi - intervallength)
            b = a + intervallength
            xx = (b-a)*(x + 1.)/2. + a # Convert the solver's (-1,1) interval into a subinterval of the (-pi,pi) interval called (a,b)
            for i in xrange(len(wavenumbers)):
                k = wavenumbers[i]
                A_c, A_s = scalefactors[i]*waveamplitudes[i, :]
                if i == 0:
                    m = 0*xx
                    A_c *= 2
                    A_s = 0
                m += A_c*np.cos(k*xx) + A_s*np.sin(k*xx)
                if i == len(wavenumbers) - 1 and setup.myosin.maxtomin is not None:
                    m = m - m.min()
                    m = (setup.myosin.maxtomin-1)*m/m.max()
                    m += 1.
        elif mode == 'randomized_bump':
            height = setup.myosin.height
            width = setup.myosin.width
            base = setup.myosin.base
            y = x/(width/2.)
            mBump = base + (height-base)*np.where(np.abs(y) < 1., np.exp(1-1/(1-y**2)), 0)
            
            np.random.seed()
            wavenumbers = np.arange(setup.myosin.maxwavenumber+1,dtype=np.float64)
            decay = setup.myosin.decay
            scalefactors = np.where(wavenumbers != 0, setup.myosin.amplitudescale/wavenumbers**decay, 1.)
            waveamplitudes = np.random.normal(0.0, 1.0, (len(wavenumbers),2))
            physicalinterval = np.float64(setup.myosin.physicalinterval) # How many um an interval of 2*pi corresponds to
            physicallength = np.float64(setup.myosin.physicallength)
            intervallength = 2*np.pi*physicallength/physicalinterval # Length of the (-pi, pi) interval over which the periodic distribution is generated to sample
            a = np.random.uniform(-np.pi, np.pi - intervallength)
            b = a + intervallength
            xx = (b-a)*(x + 1.)/2. + a # Convert the solver's (-1,1) interval into a subinterval of the (-pi,pi) interval called (a,b)
            for i in xrange(len(wavenumbers)):
                k = wavenumbers[i]
                A_c, A_s = scalefactors[i]*waveamplitudes[i, :]
                if i == 0:
                    m = 0*xx
                    A_c *= 2
                    A_s = 0
                m += A_c*np.cos(k*xx) + A_s*np.sin(k*xx)
                if i == len(wavenumbers) - 1 and setup.myosin.maxtomin is not None:
                    m = m - m.min()
                    m = (setup.myosin.maxtomin-1)*m/m.max()
                    m += 1.
            m += mBump
            
        if setup.myosin.normalize:
            mAvg = setup.myosin.normalization
            dx = x[1] - x[0]
            m = (mAvg/(0.5*dx*m.sum()))*m
            
        
        return m
        

class PrescribedMyosinSimulator(TimeIndependentSimulator):
    def _doInit(self, generator, savefolder):
        self.savefolder = savefolder
        
        initializer = initialize.Initializer(self.setup)

        x = dm.geometry.get_cell_centers(self.setup.numerical.N)
        self.x = x
        self.v = initializer.get_v()
        self.m = generator.generate(x, self.setup)
    
    def _doSolve(self):
        self.v = velocity.solve(self.m, self.setup)
    
    def _doSave(self):
        datafilename = os.path.join(self.savefolder, 'results.data')
        
        with open(datafilename, 'wb') as f:
            pickle.dump(self.x, f)
            pickle.dump(self.v, f)
            pickle.dump(self.m, f)
        
#         if self.setup.output.figs:
#             fig = plt.figure()
#             v_ax = fig.add_subplot(211)
#             m_ax = fig.add_subplot(212)
#             v_ax.plot(self.x, self.v, color='blue', linewidth=3)
#             m_ax.plot(self.x, self.m, color='red', linewidth=3)
#             v_ax.set_xlabel(r'\(x\)')
#             m_ax.set_xlabel(r'\(x\)')
#             v_ax.set_ylabel(r'\(v\)')
#             m_ax.set_ylabel(r'\(m\)')
#             figfilename = os.path.join(self.savefolder, 'plots.png')
#             fig.savefig(figfilename)
#             plt.close(fig)



def main(options = None):
    if options is None:
        options = sys.argv[1:]
    print options
    configfilename = options[0]
    label = options[1]
    '''
    If somehow the code forgot to tell main where data root is,
    try to load the sumatra project and find out
    '''
    if len(options) == 2:
        project = load_project()
        data_root = os.path.abspath(project.data_store.root)
    elif len(options) == 3:
        data_root = options[2]
    savefolder = os.path.join(data_root, label)
    gen = MyosinGenerator()
    sim = PrescribedMyosinSimulator(configfilename, gen, savefolder)
    sim.run()

if __name__ == '__main__':
    main()