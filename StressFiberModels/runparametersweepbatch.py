import subprocess
import NeuroTools.parameters as par
import time
import datetime
import os.path
import multiprocessing as mp
from sumatra.projects import load_project
from sumatra.parameters import build_parameters, YAMLParameterSet
import makemovie
import makeforceplot
import numpy as np

import runsplitsolver

project = load_project()

lock = None
pool = None

params = par.ParameterSet('/Users/fogelson/git/stressfibermodels/config.params')
physical = params.physical

sweeps = {}
sweeps['physical.alpha'] = np.linspace(0.01,10.0,20).tolist()#[0.01, 0.05, 0.1, 0.5, 1.0, 5.0]
sweeps['physical.beta'] = np.linspace(0.01,10.0,20).tolist()#[0.01, 0.05, 0.1, 0.5, 1.0, 5.0]
#sweeps['physical.beta'] = [0.1, 1.0, 10.0]
#sweeps['physical.delta'] = [0.1, 1.0, 10.0]
#sweeps['physical.gamma2'] = [0.1, 1.0, 10.0]

#outputConfigFilename = '/Users/fogelson/git/stressfibermodels/batch_generated_config.params'
outputConfigDir = '/Users/fogelson/git/stressfibermodels'

def make_label(setup):
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d-%H%M%S')
    label = timestamp + '__'
    for name in sweeps.keys():
        label += name + '_' + str(setup[name]) + '__'
    if label.endswith('__'):
        label = label[0:-2]
    label += '__force_sweep_2'
    return label

def do_movie(sweepdict, label):
    setup = par.ParameterSet(params.pretty())
    for name in sweepdict:
        setup[name] = sweepdict[name]
    subprocess.call(['python', 'StressFiberModels/makemovie.py', label])
    
def do_force_plot(sweepdict, label):
    setup = par.ParameterSet(params.pretty())
    for name in sweepdict:
        setup[name] = sweepdict[name]
    subprocess.call(['python', 'StressFiberModels/makeforceplot.py', label])

def do_sweep(sweepdict):
    global lock
    setup = par.ParameterSet(params.pretty())
    for name in sweepdict:
        setup[name] = sweepdict[name]
    label = make_label(setup)
    outputConfigFilename = os.path.join(outputConfigDir, label+'.params')
    setup.save(outputConfigFilename)
    
    with lock:
        rec = project.new_record(parameters = build_parameters(outputConfigFilename), label=label, main_file=__file__)
    
    rec.datastore.root = os.path.join(rec.datastore.root, label)
    start_time = time.time()
    runsplitsolver.main([outputConfigFilename, label])
    
    rec.duration = time.time() - start_time
    rec.output_data = rec.datastore.find_new_data(rec.timestamp)
    
    with lock:
        project.add_record(rec)
        project.save()
    
    #subprocess.call(['smt', 'run', outputConfigFilename, '--label='+label])
    #subprocess.call(['python', 'StressFiberModels/makemovie.py', label])
    #subprocess.call(['rm', outputConfigFilename])
    
    return label

def setup_sweeps(names):
    # Returns a list, each of whose entries is a dict
    # corresponding to one simulation
    if len(names) == 1:
        name = names[0]
        vals = sweeps[name]
        out = []
        for v in vals:
            out.append({name: v})
    elif len(names) > 1:
        name = names[0]
        vals = sweeps[name]
        out = []
        for v in vals:
            subout = setup_sweeps(names[1:])
            for d in subout:
                d[name] = v
            out += subout
    return out

def sweep_through(names):
    sweeps = setup_sweeps(names)
    
    numProc = 6
    global lock
    global pool
    global movieLock
    
    lock = mp.Lock()
    pool = mp.Pool(numProc)
    
    labels = pool.map(do_sweep, sweeps)
    
    setup = par.ParameterSet(params.pretty())
    
    if setup.output.movie:
        for s, l in zip(sweeps, labels):
            do_movie(s, l)
            do_force_plot(s, l)


# def sweep_through(names):
#     if len(names) == 1:
#         name = names[0]
#         vals = sweeps[name]
#         for v in vals:
#             params[name] = v
#             do_sweep()
#     elif len(names) > 1:
#         name = names[0]
#         vals = sweeps[name]
#         for v in vals:
#             params[name] = v
#             sweep_through(names[1:])

if __name__ == '__main__':
    sweep_through(sweeps.keys())