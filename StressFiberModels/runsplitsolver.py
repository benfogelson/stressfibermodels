'''
Created on Feb 1, 2014

@author: fogelson
'''

import dynamicmyosin as dm
from simulation import TimeDependentSimulator
import numpy as np
import numpy.linalg as la

import sys
import os
import os.path
import pickle
import ConfigParser
import makemovie

from dynamicmyosin.numerics import initialize


import matplotlib.pyplot as plt

class Simulator(TimeDependentSimulator):
    
    def doIsConverged(self):
        if not self.setup.simulator.convergence.on:
            return False
        order = self.setup.simulator.convergence.order
        tol = self.setup.simulator.convergence.tol
        if self.current_iteration == 0:
            return False
        else:
            vNorm = la.norm(self.v - self.vOld, order)
            mNorm = la.norm(self.m - self.mOld, order)
            nNorm = la.norm(self.n - self.nOld, order)
            norm = max(vNorm, mNorm, nNorm)
            if norm <= tol:
                return True
            else:
                return False
            
    
    def doInit(self, configfilename, savefolder):
        self.savefolder = savefolder
        x = dm.geometry.get_cell_centers(self.setup.numerical.N)
        self.x = x
        
        initializer = initialize.Initializer(self.setup)
        self.v = initializer.get_v()
        self.m = initializer.get_m()
        self.n = initializer.get_n()
        
    def initialize_variable(self, name):
        x = self.x
        try:
            val = self.__getattribute__(name)
            if type(val) == str:
                val = eval(val)
            elif type(val) == float or type(val) == int:
                val = val*np.ones(self.x.shape)
            self.__setattr__(name, val)
        except AttributeError:
            self.__setattr__(name, 0.0)
            self.initialize_variable(name)
    def doIterate(self, k):
        self.vOld, self.mOld, self.nOld = self.v, self.m, self.n
        self.v, self.m, self.n = dm.splitsolver.iterate(self.v, self.m, self.n, self.setup, k)
    def save(self):
        filename = os.path.join(self.savefolder, str(self.current_iteration) + '.data')
        
        if self.current_iteration > 0:
            normFilename = os.path.join(self.savefolder, 'norms.txt')
            vNorm = la.norm(self.v - self.vOld, self.setup.simulator.convergence.order)
            mNorm = la.norm(self.m - self.mOld, self.setup.simulator.convergence.order)
            nNorm = la.norm(self.n - self.nOld, self.setup.simulator.convergence.order)
            norm = max(vNorm, mNorm, nNorm)
            with open(normFilename, 'a') as f:
                f.write('Iteration: ' + str(self.current_iteration))
                f.write('\t\tNorm: ' + str(norm))
                f.write('\n')
                f.flush()
            
        
        with open(filename, 'wb') as f:
            pickle.dump(self.v, f)
            pickle.dump(self.m, f)
            pickle.dump(self.n, f)
        
        if self.setup.output.figs:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            vplot = ax.plot(self.x, self.v, color='blue', label = "Actin velocity")
            mplot = ax.plot(self.x, self.m, color='red', label = "Bound myosin concentration")
            nplot = ax.plot(self.x, self.n, color='green', label = "Unbound myosin concentration")
            ax.legend()
            figfilename = os.path.join(self.savefolder, str(self.current_iteration) + '.png')
            fig.savefig(figfilename)
            plt.close(fig)

def main(options = None):
    if options is None:
        options = sys.argv[1:]
    configfilename = options[0]
    label = options[1]
    data_root = '/Users/fogelson/git/stressfibermodels/Data'
    savefolder = os.path.join(data_root, label)
    sim = Simulator(configfilename, savefolder)
    sim.run()
#     # Load parameters
#     options = sys.argv
#     config_filename = options[1]
#     config = ConfigParser.SafeConfigParser()
#     config.read(config_filename)
#     iterations = config.getint("Simulation", "iterations")
#     save_freq = config.getint("Output", "save_freq")
#     
#     # Pass parameters to dynamic myosin package
#     dm.setup.loadparams(config_filename)
#     
#     # Set save location
#     label = options[-1]
#     data_root = 'Data'
#     save_folder = os.path.join(data_root, label)
#     
#     # Initialize
#     x = dm.setup.get_cell_centers(dm.setup.N)
#     v = np.zeros(x.shape)
#     m = np.ones(x.shape)
#     n = np.zeros(x.shape)
#     
#     # Pick functions for saving data and creating graphs
#     # Run solver, saving data along the way
#     
#     # Save initial values
#     saveIt(save_folder, 0, v, m, n)
#     
#     # Number of iteration blocks without saving
#     itBlocks = int(np.floor(float(iterations)/save_freq))
#     for b in xrange(1,itBlocks+1):
#         v, m, n = dm.splitsolver.iterate(v, m, n, save_freq)
#         saveIt(save_folder, b*save_freq, v, m, n)
#     # Do final few iterations if save_freq does not cleanly divide iterations
#     itFinal = iterations - itBlocks*save_freq
#     if itFinal > 0:
#         v, m, n = dm.splitsolver.iterate(v, m, n, itFinal)
#         saveIt(save_folder, iterations-1, v, m, n)
#     
#     # Save final data

if __name__ == '__main__':
    main()