from setuptools import setup, find_packages

setup(
    name='stressfiber',
    version='0.1',
    packages=['dynamicmyosin', 'simulation'],
    include_package_data=True,
    install_requires=[
        'Click',
        'numpy',
        'scipy',
        'matplotlib',
        'Sumatra',
        'NeuroTools'
    ],
    entry_points='''
        [console_scripts]
        sfbatch=dynamicmyosin.scripts.sfbatch:cli
        myscript=dynamicmyosin.scripts.myscript:main
    ''',
)