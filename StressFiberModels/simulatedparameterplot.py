'''
Created on Feb 1, 2014

@author: fogelson
'''

import operator
import os.path
import numpy as np
import NeuroTools.parameters as par
import sys
import matplotlib.pyplot as plt
import processbatch as pb
import pickle
from scipy import interpolate

def main():
    options = sys.argv[1:]
    setup = par.ParameterSet(options[0])
    label = options[1]
    
    with open('./Data/20140316-113827/force_sweep_2_forces.data', 'rb') as f:
        A = pickle.load(f)
        B = pickle.load(f)
        Fl = pickle.load(f)
        Fr = pickle.load(f)

    savefolder = os.path.join('./Data', label)
    
    plt.pcolor(A, B, Fl)
    plt.colorbar()
    figfilename = os.path.join(savefolder, 'F_left.pdf')
    plt.savefig(figfilename)
    plt.close()
    
    plt.pcolor(A, B, Fr)
    plt.colorbar()
    figfilename = os.path.join(savefolder, 'F_right.pdf')
    plt.savefig(figfilename)
    plt.close()
    
    
    

if __name__ == '__main__':
    main()