import petsc4py
from contextlib2 import ExitStack
import numpy as np


def upwind_limiter(theta, phi):
    phi[...] = 0


def lax_wendroff_limiter(theta, phi):
    phi[...] = 1.0


def superbee_limiter(theta, phi):
    phi[...] = np.maximum(np.maximum(0, np.minimum(1, 2*theta)), np.minimum(2, theta))


class ActinMyosinSystem(object):
    def __init__(self,
                 nx,
                 alpha,
                 betal,
                 betar,
                 gamma,
                 gammabound,
                 kappaon,
                 kappaoff,
                 limiter):
        self.nx = nx
        self.h = 1.0 / nx
        self.h2 = self.h ** 2
        self.alpha = alpha
        self.betal = betal
        self.betar = betar
        self.gamma = gamma
        self.gammabound = gammabound
        self.kappaon = kappaon
        self.kappaoff = kappaoff

        if limiter == 'upwind':
            self.limiter = upwind_limiter
        elif limiter == 'lax_wondroff':
            self.limiter = lax_wendroff_limiter
        elif limiter == 'superbee':
            self.limiter = superbee_limiter
        else:
            raise ValueError('Unsupported or unknown limiter name.')

        self.daCell = petsc4py.PETSc.DMDA().create([nx], boundary_type='ghosted', stencil_width=2)
        self.daCell.setName('daCell')

        self.daFace = petsc4py.PETSc.DMDA().create([nx+1])
        self.daFace.setName('daFace')

        # Scratch associated with slope-limited flux
        self._Vface = self.daFace.createGlobalVector()
        self._Theta = self.daFace.createGlobalVector()
        self._Mjump = self.daFace.createGlobalVector()
        self._MjumpUW = self.daFace.createGlobalVector()
        self._Phi = self.daFace.createGlobalVector()
        self._JM = self.daFace.createGlobalVector()


        self.daComp = petsc4py.PETSc.DMComposite().create()
        self.daComp.addDM(self.daCell)
        self.daComp.addDM(self.daCell)
        self.daComp.addDM(self.daCell)
        self.daComp.setName('daComp')

        self.Xcell = self.daCell.createGlobalVector()
        with self.Xcell as x:
            x[...] = np.linspace(-0.5 + self.h/2, 0.5 - self.h/2, self.nx)

        self.Xface = self.daFace.createGlobalVector()
        with self.Xface as x:
            x[...] = np.linspace(-0.5, 0.5, self.nx + 1)

    def evaluate_implicit_function(self, ts, t, U, Udot, FU):
        with ExitStack() as stack:
            V, M, N = stack.enter_context(self.daComp.getAccess(U))
            Vdot, Mdot, Ndot = stack.enter_context(self.daComp.getAccess(Udot))
            FV, FM, FN = stack.enter_context(self.daComp.getAccess(FU))

            v = stack.enter_context(V)
            m = stack.enter_context(M)
            n = stack.enter_context(N)

            mdot = stack.enter_context(Mdot)
            ndot = stack.enter_context(Ndot)

            fv = stack.enter_context(FV)
            fm = stack.enter_context(FM)
            fn = stack.enter_context(FN)

            fv[...] = 0
            fm[...] = 0
            fn[...] = 0

            self.compute_stress_function(v, m, fv)
            self.compute_myosin_reaction_function(m, n, fm, fn)
            self.compute_diffusion_function(n, fn, self.gamma)
            if self.gammabound != 0:
                self.compute_diffusion_function(m, fm, self.gammabound)

            fm[...] = mdot[...] - fm[...]
            fn[...] = ndot[...] - fn[...]

    def evaluate_rhs_function(self, ts, t, U, GU):
        with ExitStack() as stack:
            V, M, N = stack.enter_context(self.daComp.getAccess(U))
            GV, GM, GN = stack.enter_context(self.daComp.getAccess(GU))

            v = stack.enter_context(V)
            m = stack.enter_context(M)
            n = stack.enter_context(N)

            gv = stack.enter_context(GV)
            gm = stack.enter_context(GM)
            gn = stack.enter_context(GN)

            gv[...] = 0
            gm[...] = 0
            gn[...] = 0

            # self.compute_myosin_reaction_function(m, n, gm, gn)
            self.compute_bound_myosin_advection_function(v, m, gm)

    def compute_bound_myosin_advection_function(self, v, m, fm):
        """
        Semi-discrete reconstruction algorithm:
            - Interpolate v from cell-centered values to velocities at faces
            - Compute a slope-limited linear reconstruction of m
            - At each face, use the linear reconstruction in the upwind cell to compute m at the face
            - Compute the current flux using that value of m
            - Apply fluxes in and out of each cell

        Algorithm assumes that v is positive at the left boundary and negative at the right boundary, so that
        there is no outflow to be dealt with. This means enforcing the no flux boundary condition is a simple as
        no including flux terms for the boundary cell faces.

        :param v: Cell-centered velocity values (input)
        :param m: Cell-centered (cell-averaged) bound myosin concentration (input)
        :param fm: Adds rate of change of myosin to each cell (net flux divided by cell width) (output)
        :return:
        """
        nx = self.nx
        hinv = 1.0/self.h
        limiter = self.limiter

        with ExitStack() as stack:
            vface = stack.enter_context(self._Vface)
            theta = stack.enter_context(self._Theta)
            mjump = stack.enter_context(self._Mjump)
            phi = stack.enter_context(self._Phi)
            jm = stack.enter_context(self._JM)

            # Interpolate velocity values to cell faces
            vface[1:nx] = 0.5*(v[1:nx] + v[0:nx-1])
            vface[0] = 0
            vface[nx] = 0

            # Compute ratio of slope across face to slope in upwind direction
            mjump[0] = 0
            mjump[nx] = 0
            mjump[1:nx] = m[1:nx] - m[0:nx-1]
            theta[1:nx] = np.where(vface[1:nx] > 0,
                                   mjump[0:nx-1]/mjump[1:nx],
                                   mjump[2:nx+1]/mjump[1:nx])

            # Compute limiter
            limiter(theta, phi)

            # Interpolate m from upwind side of each face using a limited slope
            # Multiply m by vface to get the flux jm at that face
            jm[1:nx] = np.where(vface[1:nx] > 0,
                                m[0:nx-1] + 0.5*phi[1:nx]*mjump[1:nx],
                                m[1:nx]   - 0.5*phi[1:nx]*mjump[1:nx])
            jm[1:nx] *= vface[1:nx]
            jm[0] = 0
            jm[nx] = 0

            # Apply flux differencing at interior faces
            fm[1:nx] +=  hinv*jm[1:nx]
            fm[0:nx-1] += -hinv*jm[1:nx]

    def compute_diffusion_function(self, u, fu, coeff):
        nx = self.nx
        h2inv = 1.0 / self.h2

        fu[1:nx - 1] += coeff * h2inv * (u[2:nx] - 2 * u[1:nx - 1] + u[0:nx - 2])
        fu[0] += coeff * h2inv * (u[1] - u[0])
        fu[nx-1] += -coeff * h2inv * (u[nx-1] - u[nx-2])

    def compute_myosin_reaction_function(self, m, n, fm, fn):
        kappaon = self.kappaon
        kappaoff = self.kappaoff

        fm[...] += kappaon * n[...] - kappaoff * m[...]
        fn[...] += kappaoff * m[...] - kappaon * n[...]

    def compute_stress_function(self, v, m, fv):
        alpha = self.alpha
        alpha2 = alpha ** 2
        betal = self.betal
        betar = self.betar
        nx = self.nx
        hinv = 1.0 / self.h
        h2inv = 1.0 / self.h2

        fv[1:nx - 1] += (h2inv * (v[2:nx] - 2 * v[1:nx - 1] + v[0:nx - 2])
                         + 0.5 * hinv * (m[2:nx] - m[0:nx - 2])
                         - alpha2 * v[1:nx - 1])


        # Ghost values of v from boundary condition
        # This uses a one-sided linear extrapolation of m from interior cells to the boundary
        # and a standard centered finite difference of v' centered around the boundary
        vl = (v[0]*(hinv - 0.5*betal) + 0.5*(3*m[0]-m[1]))/(hinv + 0.5*betal)
        vr = (v[nx-1]*(hinv - 0.5*betar) - 0.5*(3*m[nx-1] - m[nx-2]))/(hinv + 0.5*betar)

        # The finite difference stencil for m is also modified at the boundaries to be one-sided
        # in its approximation of m'
        fv[0] += (h2inv * (v[1] - 2 * v[0] + vl)
                  + hinv * (m[1] - m[0])
                  - alpha2 * v[0])

        fv[nx - 1] += (h2inv * (v[nx-2] - 2 * v[nx-1] + vr)
                       + hinv * (m[nx-1] - m[nx-2])
                       - alpha2 * v[nx-1])
