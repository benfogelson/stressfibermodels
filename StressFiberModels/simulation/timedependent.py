'''
Created on Feb 2, 2014

@author: fogelson
'''
from __future__ import print_function
import abc
import NeuroTools.parameters as par

class TimeIndependentSimulator(object):
    __metaclass__ = abc.ABCMeta
    
    def __init__(self, setup, *args, **kwargs):
        if type(setup) == str:
            setup = par.ParameterSet(setup)
        self.setup = setup
        
        self._doInit(*args, **kwargs)
    
    @abc.abstractmethod
    def _doInit(self, *args, **kwargs):
        pass
    
    def solve(self):
        self._doSolve()
        
    @abc.abstractmethod
    def _doSolve(self):
        pass

    def save(self):
        self._doSave()
    
    @abc.abstractmethod
    def _doSave(self):
        pass
    
    def run(self):
        self.solve()
        self.save()
    
class TimeDependentSimulator(object):
    '''
    Class for handling simulations that iterate through time.
    
    Helps streamline the process of config file reading,
    iterations, and saving output.
    '''
    __metaclass__ = abc.ABCMeta

#     @abc.abstractmethod
#     def __init__(self, configfilename, configfilesections=['Simulator']):
#         self.configfilesections = configfilesections
#         self.current_iteration = 0
#         self.loadconfig(configfilename)
    
    def __init__(self, setup, *args, **kwargs):
        '''
        Initialize the simulator. Setup can be either a
        NeuroTools.parameters.ParameterSet object, or a string
        to the file location or URL where such an object is
        saved.
        '''
        self.current_iteration = 0
        
        if type(setup) == str:
            setup = par.ParameterSet(setup)
        self.setup = setup
        
        self.doInit(setup, *args, **kwargs)
    
    def isConverged(self):
        return self.doIsConverged()
    
    def doIsConverged(self):
        '''
        Method, designed to be overridden, to allow for simulations to
        stop early in the event of convergence of some type.
        '''
        return False
    
    @abc.abstractmethod
    def doInit(self, *args, **kwargs):
        '''
        Init method, designed to be overridden, so that subclasses
        do not have to remember to call the __init__ super method
        of this abstract base class.
        '''
        pass
        
    def iterate(self, k):
        self.current_iteration += k
        self.doIterate(k)
    
    @abc.abstractmethod
    def doIterate(self, k):
        pass
    
    def run(self):
        self.save()
        while self.current_iteration < self.setup.simulator.iterations and not self.isConverged():
            self.iterate_to_next_save()
            self.save()
        if self.isConverged():
            self.save()
    
    def iterate_to_next_save(self):
        '''
        Iterates to the next save point, which is either
        current_iteration + block_size or the final iteration,
        if current_iteration + block_size is past the end
        of the simulation.
        '''
        block_size = self.setup.simulator.block_size
        iterations = self.setup.simulator.iterations
        next_block = self.current_iteration + block_size
        
        if next_block < iterations:
            k = block_size
        else:
            k = iterations - self.current_iteration
        # Just in case something bad happens with the subtraction on the line above
        k = max(k, 0)
        self.iterate(k)
    
    @abc.abstractmethod
    def save(self):
        pass
    
#     def loadconfig(self, filename):
#         '''
#         Reads a config file, and imports all
#         settings in the sections given by self.section_name.
#         
#         By default, this is just the [Simulator] section
#         '''
#         config = ConfigParser.SafeConfigParser()
#         # Set config to be case sensitive
#         config.optionxform = str
#         config.read(filename)
#         for section_name in self.configfilesections:
#             if config.has_section(section_name):
#                 opts = config.options(section_name)
#                 for o in opts:
#                     if not o.startswith('_'):
#                         try:
#                             self.__dict__[o] = ast.literal_eval(config.get(section_name, o))
#                         except ValueError:
#                             self.__dict__[o] = config.get(section_name, o)

class DummySimulator(TimeDependentSimulator):
    def doInit(self, k):
        pass
    
    def doIterate(self, k):
        pass
        
    def save(self):
        print(self.current_iteration)