{
  "batch": {
    "batchclass": "Alpha",
  },
  "sweep": {
    "flatSweep": True,
    "batchname": "force_scaling_randomized_bump_run2",
    "numproc": 8,
    "force": {
      "plot": True,
      "plotaverage": True,
      "xvar": 'myosin.physicallength',
    },
  },
  "initial": {
    "m": "randomized_bump",
  },
  "output": {
    "figs": True,
    "movie": False,
  },
  "simulator": {
    "block_size": 200,
    "iterations": 80000,
    "convergence": {
      "on": True,
      "order": 2,
      "tol": 1e-4,
    },
  },
  "numerical": {
    "N": 1024,
    "deltaT": 0.01,
    "solvers": {
      "myosinadvection": "High-res",
      "myosin": "TR-BDF2",
    },
  },
  "physical": {
    "delta_small": 0.01,
    "gamma1": 0.1,
    "beta": 'eval: par.ParameterRange(np.kron(np.linspace(.1,5.,5), np.ones((30,))))', # If beta exists, it overrides beta1 and beta2
    "beta2": 1.0,
    "beta1": 1.0,
    "delta": 0.1,
    "alpha": 'eval: par.ParameterRange(np.kron(np.linspace(.1,20.,5), np.ones((30,))))',
    "gamma2": 0.1,
  },
  "convergence": {
    "processes": 8,
    "solver": "split",
    "vars": {
      "deltaT": {
        "name": "numerical.deltaT",
        "vals": map(lambda p: 0.01/(2.0**p), range(8)),
      },
      "iterations": {
        "name": "simulator.iterations",
        "vals": map(lambda p: int(50*(2.0**p)), range(8)),
      },
      #"N": {
      #  "name": "numerical.N",
      #  "vals": [32, 64, 128, 256, 512, 1024, 2048, 4096],
      #},
    },
  },
  "myosin": {
    "mode": {
      "random": 0.5,
      "central_bump": 0.5,
    },
    "c": 1.0,
    "height": 2.0,
    "width": 2.0,
    "base": 1.0,
    "normalize": True,
    "normalization": 1.0,
    "maxwavenumber": 200,
    "decay": 1.0,
    "amplitudescale": 1.,
    "physicalinterval": 100.,   # Length in um of the full (-pi,pi) interval over which a random distribution is generated
    "physicallength": 'eval: par.ParameterRange(np.kron(np.linspace(25., 70., 5), np.ones((30,))))',      # Length in um of the physical myosin distribution we want to generate
    "maxtomin": 2,
  },
}