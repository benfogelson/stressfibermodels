This repository provides application code for solving the model equations for force generation in contractile fibers as described in

Fogelson, B., & Mogilner, A. (2018). Actin-Myosin Force Generation and Symmetry Breaking in the Model Contractile Fiber. SIAM Journal on Applied Mathematics, 78(3), 1754–1777. https://doi.org/10.1137/17M1112261
