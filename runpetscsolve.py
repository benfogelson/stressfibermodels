import click
import pypet
import yaml
import petsc4py
import os.path
import os
import numpy as np
import matplotlib.pyplot as plt
from contextlib import ExitStack

from simulation import ActinMyosinSystem


@click.command()
@click.argument('configfile', type=click.Path(exists=True, dir_okay=False))
def main(configfile: str):
    with open(configfile, 'r') as f:
        config = yaml.load(f)

    sumatra_label = config['sumatra_label']
    data_path = os.path.join('./Data', sumatra_label)

    petscargs = []
    try:
        petsc = config['petsc']
        for opt in petsc:
            petscargs.append('-{0}'.format(opt))
            petscargs.append(petsc[opt])
    except KeyError:
        pass
    petsc4py.init(petscargs)

    env = pypet.Environment(trajectory=sumatra_label,
                            overwrite_file=True,
                            filename=data_path)

    traj = env.trajectory
    add_config_to_parameters(traj, config)

    K = traj.kappaon/traj.kappaoff
    traj.f_add_derived_parameter('K', K)

    # L = np.linspace(0.1, 12, 4)
    # traj.f_explore({'alpha': (L/2).tolist(),
    #                 'betal': (L/10).tolist(),
    #                 'betar': (L/10).tolist(),
    #                 'gamma': ((K+1)/(L**2)).tolist()
    #                 })

    env.run(run_simulation, data_path)


def add_config_to_parameters(group, config):
    for k in config:
        if type(config[k]) is dict:
            subgroup = group.f_add_parameter_group(k)
            add_config_to_parameters(subgroup, config[k])
        else:
            group.f_add_parameter(k, config[k])


def run_simulation(traj: pypet.Trajectory, root_data_path):
    run_name = traj.f_idx_to_run(traj.v_idx)
    data_path = os.path.join(root_data_path, run_name)
    os.mkdir(data_path)

    problem = ActinMyosinSystem(
        nx = traj.numerics.nx,
        alpha = traj.model.alpha,
        betal = traj.model.betal,
        betar = traj.model.betar,
        gamma = traj.model.gamma,
        gammabound = traj.model.gammabound,
        kappaon = traj.model.kappaon,
        kappaoff = traj.model.kappaoff,
        limiter = traj.numerics.limiter
    )

    ts = petsc4py.PETSc.TS().create()
    ts.setDM(problem.daComp)
    ts.setTimeStep(traj.numerics.dt)
    ts.setIFunction(problem.evaluate_implicit_function)
    ts.setRHSFunction(problem.evaluate_rhs_function)
    ts.setFromOptions()

    save_times = np.linspace(0, traj.simulation.final_time, traj.number_of_saves)
    save_count = 0

    U = problem.daComp.createGlobalVector()
    Uold = U.duplicate()
    with problem.daComp.getAccess(U) as (V, M, N):
        with M as m, N as n:
            m[...] = np.random.rand(traj.numerics.nx)
    U.copy(Uold)

    converged = False
    save_snapshot(traj, problem, ts, save_count, U, data_path)
    for save_count in range(1, traj.number_of_saves):
        t = save_times[save_count]
        ts.setDuration(t, traj.simulation.max_steps)
        ts.solve(U)
        save_snapshot(traj, problem, ts, save_count, U, data_path)
        if is_converged(traj, problem, U, Uold):
            converged = True
            break
        else:
            pass
            U.copy(Uold)

    traj.f_add_result('converged', converged)


def is_converged(traj, problem, U, Uold):
    tol = float(traj.simulation.convergence_tol)
    ord = traj.simulation.convergence_ord
    if ord == 'inf':
        ord = np.inf

    h = problem.h

    with ExitStack() as stack:
        V, M, N = stack.enter_context(problem.daComp.getAccess(U))
        Vold, Mold, Nold = stack.enter_context(problem.daComp.getAccess(Uold))

        v = stack.enter_context(V)
        m = stack.enter_context(M)
        n = stack.enter_context(N)
        vold = stack.enter_context(Vold)
        nold = stack.enter_context(Nold)
        mold = stack.enter_context(Mold)

        if ord != np.inf:
            vnorm = np.power(h, 1 / ord) * np.linalg.norm(v - vold, ord)
            mnorm = np.power(h, 1 / ord) * np.linalg.norm(m - mold, ord)
            nnorm = np.power(h, 1 / ord) * np.linalg.norm(n - nold, ord)
        else:
            vnorm = np.linalg.norm(v - vold, ord)
            mnorm = np.linalg.norm(m - mold, ord)
            nnorm = np.linalg.norm(n - nold, ord)

        if vnorm < tol and mnorm < tol and nnorm < tol:
            return True
        else:
            return False


def save_snapshot(traj, problem, ts, save_count, U, data_path):
    t = ts.getTime()

    if traj.output.save_figures:
        with problem.daComp.getAccess(U) as (V, M, N):
            with V as v, M as m, N as n, problem.Xcell as x:
                fig = plt.figure()
                ax = fig.add_subplot(111)
                ax.plot(x, v)
                ax.plot(x, m)
                ax.plot(x, n)
                fig.savefig(os.path.join(data_path, '{:0>5d}.png'.format(save_count)))
                plt.close(fig)

    if traj.output.save_binaries:
        binary_path = os.path.join(data_path, '{:0>5d}.dat'.format(save_count))
        if os.path.exists(binary_path):
            mode = petsc4py.PETSc.Viewer.Mode.APPEND
        else:
            mode = petsc4py.PETSc.Viewer.Mode.WRITE

        viewer = petsc4py.PETSc.Viewer()
        viewer = viewer.createBinary(binary_path, mode=mode)
        U.view(viewer)


if __name__ == '__main__':
    main()

