# coding: utf-8
import processbatch as pb
get_ipython().magic(u'cd git/stressfibermodels/StressFiberModels/')
import processbatch as pb
from sumatra.projects import load_project
from sumatra.projects import load_project
get_ipython().magic(u'cd ..')
project = load_project()
labels = project.record_store.labels(project.name)
labelEnds = map(lambda l: l[17:], labels)
alphas = np.linspace(0.01,10.0,20).tolist()
betas = np.linspace(0.01,10.0,20).tolist()
Na = len(alphas)
Nb = len(betas)
import pickle
get_ipython().magic(u'cd Data/20140316-113827/')
with open('force_sweep_2_forces.data', 'rb') as f:
    A = pickle.load(f)
    B = pickle.load(f)
    Fl = pickle.load(f)
    Fr = pickle.load(f)
    
get_ipython().magic(u'cd ../..')
from scipy import interpolate
interpolate.BivariateSpline()
spl = interpolate.BivariateSpline()
spl = interpolate.SmoothBivariateSpline(A, B, Fl, s=0)
spl = interpolate.SmoothBivariateSpline(alphas, betas, Fl, s=0)
spl = interpolate.SmoothBivariateSpline(?
get_ipython().set_next_input(u'spl = interpolate.SmoothBivariateSpline');get_ipython().magic(u'pinfo interpolate.SmoothBivariateSpline')
spl = interpolate.SmoothBivariateSpline
spl = interpolate.SmoothBivariateSpline(reshape(A, (-1)), reshape(B, (-1)), reshape(Fl, (-1)))
spl
pcolor(A, B, spl(A, B))
pcolor(A, B, spl.ev(A, B))
spl = interpolate.RectBivariateSpline(alphas, betas, Fl)
spl = interpolate.RectBivariateSpline(alphas, betas, Fl)
spl = interpolate.RectBivariateSpline(alphas, betas, Fl)
spl(A, B)
spl(alphas)
spl(alphas, betas)
shape(_)
pcolor(spl(alphas, betas))
AA, BB = meshgrid(alphas, betas)
AA
A
spl = interpolate.RectBivariateSpline(alphas, betas, Fl.T)
pcolor(spl(alphas, betas))
spl(.3, .1)
spl([1., 2., 3.], [0., 0., 0.])
pcolor(spl(alphas, betas))
pcolor(spl(linspace(0,10,100), linspace(0,10,100)))
a = lambda xi, L, mu: xi*(L**2)/mu
b = lambda zeta, L, mu: zeta*L/mu
pcolor(AA, BB, spl(AA, BB))
pcolor(AA, BB, spl(alphas, betas))
alphas
betas
rbf = interpolate.Rbf(A, B, Fl)
pcolor(A, B, rbf(A, B))
pcolor(A, B, rbf(A, B))
rbf = interpolate.Rbf(A, B, rbf(A, B)?
get_ipython().set_next_input(u'rbf = interpolate.Rbf');get_ipython().magic(u'pinfo interpolate.Rbf')
rbf = interpolate.Rbf(A, B, Fl, s = 0)
pcolor(A, B, rbf(A, B))
pcolor(A, B, rbf(A, B) - Fl)
colorbar()
pcolor(AA, BB, spl(alphas, betas))
a
b
mu = 1.0
zeta = 1.0
xi = 1.0
L = linspace(10., 70.)
a(xi, L, mu)
mu = 50
a(xi, L, mu)
mu = 500
a(xi, L, mu)
b(zeta, L, mu)
zeta = 1000
b(zeta, L, mu)
zeta = 100
b(zeta, L, mu)
a(xi, L, mu)
zeta
mu
xi
L
a(xi, L, mu)
xi = 0.1
a(xi, L, mu)
mu = 50
a(xi, L, mu)
b(zeta, L, mu)
zeta = 10.0
b(zeta, L, mu)
a(xi, L, mu)
plot(a(xi, L, mu), b(zeta, L, mu))
zeta
zeta = 1.0
b(zeta, L, mu)
plot(a(xi, L, mu), b(zeta, L, mu))
zeta = 5.0
xi = 0.05
plot(a(xi, L, mu), b(zeta, L, mu))
pcolor(A, B, Fl)
plot(a(xi, L, mu), b(zeta, L, mu), color='black', linewidth=3)
zeta
zeta = 10.0
xi = 0.1
plot(a(xi, L, mu), b(zeta, L, mu), color='black', linewidth=3)
pcolor(A, B, Fl)
plot(a(xi, L, mu), b(zeta, L, mu), color='black', linewidth=3)
figure()
spl(a(xi, L, mu), b(zeta,L,mu))
shape(_)
diag(spl(a(xi, L, mu), b(zeta,L,mu)))
plot(L, _)
a = lambda xi, L, mu: xi*(L**(2.5))/mu
plot(a(xi, L, mu), b(zeta, L, mu), color='black', linewidth=3)
diag(spl(a(xi, L, mu), b(zeta,L,mu)))
plot(L, _)
spl2 = interpolate.RectBivariateSpline(alphas, betas, Fr.T)
diag(spl2(a(xi, L, mu), b(zeta,L,mu)))
plot(a(xi, L, mu), b(zeta, L, mu), color='black', linewidth=3)
diag(spl2(a(xi, L, mu), b(zeta,L,mu)))
plot(L, _)
diag(spl2(a(xi, L, mu), b(zeta,L,mu)))
plot(L, _)
pcolor(A, B, Fr)
plot(a(xi, L, mu), b(zeta, L, mu), color='black', linewidth=3)
Xi = [0.1, 1.0, 10.]
Mu = [0.1, 1.0, 10.]
Zeta = [0.1, 1.0, 10.]
a = lambda xi, L, mu: xi*(L**2)/mu
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            plot(L, f)
            
Xi = [0.01, 1.0, 10.]
Mu = [0.01, 0.1, 1.0, 10.]
Zeta = [0.01, 0.1, 1.0, 10.]
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            plot(L, f)
            
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            plot(a(xi, L, mu), b(zeta, L, mu))
            
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) < 20 and max(b(xi, L, mu)) < 20:
                print xi, mu, zeta
                
Mu = [10., 100.0, 1000.0]
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) < 20 and max(b(xi, L, mu)) < 20:
                print xi, mu, zeta
                
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            plot(L, f)
            
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(L, f)
                
a = lambda xi, L, mu: xi*(L**2.5)/mu
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(L, f)
                
Xi = [0.01, 1.0, 10., 100.0]
Zeta = [0.01, 0.1, 1.0, 10., 100.0]
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(L, f)
                
Mu = [1.0, 10., 100.0, 1000.0]
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(L, f)
                
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(a(xi, L, mu), b(xi, L, mu))
                
pcolor(A, B, Fr)
pcolor(A, B, Fl)
L = linspace(0, 1)
Xi = [0.01, 1.0, 10., 100.0]
Xi = array([0.01, 1.0, 10., 100.0])
Zeta = 2*Xi
Mu = [1.0, 10., 100.0, 1000.0]
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(a(xi, L, mu), b(xi, L, mu))
                
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(a(xi, L, mu), b(xi, L, mu))
                
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                plot(a(xi, L, mu), b(xi, L, mu))
                plot(L, f)
                
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            if max(a(xi, L, mu)) <= 10 and max(b(xi, L, mu)) <= 10:
                #plot(a(xi, L, mu), b(xi, L, mu))
                plot(L, f)
                
for xi in Xi:
    for mu in Mu:
        for zeta in Zeta:
            f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
            plot(L, f)
            
Xi
for xi in Xi:
    mu = xi/10.0
    zeta = 2.0*xi
    f = diag(spl(a(xi, L, mu), b(zeta, L, mu)))
    plot(L, f)
    
Xi
mu
zeta
pcolor(A, B, Fl)
plot(10*L, sqrt(10*L))
plot(10*L, 2*sqrt(10*L))
plot(10*L, 4*sqrt(10*L))
L = linspace(0,10)
pcolor(A, B, Fl)
xlim(0,10)
ylim(0,10)
plot(L**(2.5), L)
plot((2*L)**(2.5), L)
plot((L)**(2.5), 2*L)
plot((L)**(2.5), 4*L)
f = diag(L**2.5, 4*L)
f = diag(spl(L**2.5, 4*L))
figure()
plot(L, f)
plot(L**2.5, 4*L)
plot(L**2.5, 4*L, linewidth=3)
figure(1)
plot(L**2.5, 4*L, linewidth=3)
a = L**2.5
b = 4*L
for i in xrange(len(a)):
    f = spl(a[i], b[i])
    plot(L[i], f, 'bo')
    
for i in xrange(len(a)):
    f = spl(a[i], b[i])
    plot(L[i], f, 'bo')
    
for i in xrange(len(a)):
    f = spl(a[i], b[i])
    plot(L[i], f, 'bo')
    
spl = interpolate.RectBivariateSpline(alphas, betas, Fl.T)
spl(A, B)
spl = interpolate.SmoothBivariateSpline(A[:], B[:], Fl[:], s=0.0)
A[:]
spl = interpolate.SmoothBivariateSpline(reshape(A,(-1)), B[:], Fl[:], s=0.0)
spl = interpolate.SmoothBivariateSpline(reshape(A,(-1)), reshape(B,(-1)), reshape(Fl,(-1)), s=0.0)
pcolor(A, B, spl(A, B))
spl(1.0, 1.0)
for i in xrange(len(a)):
    f = spl(a[i], b[i])
    plot(L[i], f, 'bo')
    
spl = interpolate.LSQBivariateSpline(reshape(A,(-1)), reshape(B,(-1)), reshape(Fl,(-1)), s=0.0)
spl = interpolate.LSQBivariateSpline(reshape(A,(-1)), reshape(B,(-1)), reshape(Fl,(-1)))
spl = interpolate.SmoothBivariateSpline(reshape(A,(-1)), reshape(B,(-1)), reshape(Fl,(-1)))
for i in xrange(len(a)):
    f = spl(a[i], b[i])
    plot(L[i], f, 'bo')
    
figure()
a
b
a = L**2
for i in xrange(len(a)):
    f = spl(a[i], b[i])
    plot(L[i], f, 'bo')
    
f = []
for i in xrange(len(a)):
    f.append(spl(a[i], b[i])[0])
    
plot(L, f)
figure()
plot(a, b)
L
b = 4*L
plot(a, b)
figure()
plot(L, f)
pcolor(A, B, Fl)
close('all')
pcolor(A, B, Fl)
xlabel('Drag in fiber (alpha)')
ylabel('Strength of adhesions (beta)')
get_ipython().magic(u'cd ~/Dropbox')
get_ipython().magic(u'cd Share\\ Folder')
get_ipython().system(u'ls -F ')
get_ipython().system(u'mkdir Alex_Email')
get_ipython().magic(u'cd Alex_Email/')
savefig('Figure 1.png')
a = L**2
b = 4*L
plot(a, b)
xlim(0,10)
ylim(0,10)
plot(a, b, linewidth=3, color='black')
plot(L**2, 6*L, linewidth=3, color='black')
plot(L**2, 2*L, linewidth=3, color='black')
savefig('Figure 2.png')
f = []
a = L**2
b = 2*L
figure()
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        
f = []
l = []
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        l.append(L[i])
        
plot(l, f, linewidth=3, color='black')
xlabel('Length (L)')
ylabel('Force')
xlabel('Length (L)')
ylabel('Force')
a = L**2
b = 4*L
f = []
l = []
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        l.append(L[i])
        
plot(l, f, linewidth=3, color='black')
f = []
l = []
b = 6*L
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        l.append(L[i])
        
plot(l, f, linewidth=3, color='black')
figure(2);
savefig('Figure 3.png')
close('all')
pcolor(A, B, Fr)
savefig('Figure 4.png')
close()
pcolor(A, B, (Fl - Fr)/Fl)
colorbar()
savefig('Figure 5.png')
xlabel('Drag in fiber (alpha)')
ylabel('Strength of adhesions (beta)')
savefig('Figure 5.png')
close()
pcolor(A, B, Fr)
xlabel('Drag in fiber (alpha)')
ylabel('Strength of adhesions (beta)')
savefig('Figure 4.png')
title('Force at right edge of fiber')
title('Force at right edge of fiber (far edge)')
savefig('Figure 4.png')
close()
pcolor(A, B, (Fl - Fr)/Fl)
xlabel('Drag in fiber (alpha)')
ylabel('Strength of adhesions (beta)')
title('Relative difference between forces at fiber ends')
colorbar()
savefig('Figure 5.png')
close()
pcolor(A, B, Fl)
xlabel('Drag in fiber (alpha)')
ylabel('Strength of adhesions (beta)')
title('Force at left edge of fiber (near edge)')
savefig('Figure 1.png')
close()
pcolor(A, B, Fl)
xlabel('Drag in fiber (alpha)')
ylabel('Strength of adhesions (beta)')
title('Force at left edge of fiber (near edge)')
plot(L**2, 2*L, linewidth=3, color='black')
plot(L**2, 4*L, linewidth=3, color='black')
plot(L**2, 6*L, linewidth=3, color='black')
xlim(0,10)
ylim(0,10)
savefig('Figure 2.png')
close()
f = []
l = []
a = L**2
b = 6*L
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        l.append(L[i])
        
plot(l, f, linewidth=3, color='black')
f = []
l = []
b = 4*L
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        l.append(L[i])
        
plot(l, f, linewidth=3, color='black')
f = []
l = []
b = 4*L
b = 2*L
for i in xrange(len(a)):
    if a[i] <= 10 and b[i] <= 10:
        f.append(spl(a[i], b[i])[0])
        l.append(L[i])
        
plot(l, f, linewidth=3, color='black')
xlabel('Length (L)')
ylabel('Force')
title('Force vs length')
title('Force vs length, showing force peak')
savefig('Figure 3.png')
